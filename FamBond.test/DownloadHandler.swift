//
//  DownloadHandler.swift
//  FamBond.test
//
//  Created by Jatin Garg on 29/01/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import Foundation
import UIKit

protocol DownloadHandlerDelegate: class{
    func didProceedWithProgress(progress: Double, message: Message_CD)
    func didCompleteDownload(location: URL,fileName: String?,message: Message_CD)
}
enum MediaType: String{
    case video = "video"
    case image = "image"
    case text = "text"
}

class DownloadHandler:NSObject, URLSessionDownloadDelegate{
    var message: Message_CD!
    var url: URL!
    var type:MediaType!
    weak var delegate: DownloadHandlerDelegate? = nil
    init(dictionary: [String : Any]){
        super.init()
        self.message = dictionary["message"] as! Message_CD
        self.url = dictionary["url"] as! URL
        self.type = dictionary["type"] as! MediaType
    }
    
    func startDownload(){
        let req = URLRequest(url: url)
        let session = URLSession(configuration: .ephemeral, delegate: self, delegateQueue: OperationQueue.main )
        let task = session.downloadTask(with: req)
        task.resume()
    }
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        let suggestedName = downloadTask.response?.suggestedFilename
        let ext = (NSString(string: suggestedName!)).pathExtension
        let fileName = "\(NSUUID().uuidString).\(ext)"
        delegate?.didCompleteDownload(location: location, fileName: fileName,message: message)
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        let progress = Float(totalBytesWritten)/Float(totalBytesExpectedToWrite)
        delegate?.didProceedWithProgress(progress: Double(progress), message: message)
    }
}
