//
//  NewBondVC.swift
//  FamBond.test
//
//  Created by Jatin Garg on 24/02/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import UIKit
import Alamofire
import CoreData
import SDWebImage

class NewBondCell: UITableViewCell{

    @IBOutlet weak var profilePicture: UIImageView!
    @IBOutlet weak var nameLabel: PriorityLabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        nameLabel.isSelected = false
    }
    

}
class NewBondVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var bondPicture: UIImageView!
    @IBOutlet weak var bondName: FTextField!
    var url: String = ""
    var privateCtx = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
    var selectedContacts = [String](){
        didSet{
            statusLabel.text = "\(selectedContacts.count)"
        }
    }

    var contacts: [Contact]? = nil{
        didSet{
            if contacts != nil{
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }

            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        privateCtx.parent = DBManager.getContext()
        getContacts()

    }

    @IBAction func editPicturePressed(_ sender: Any) {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = true
        picker.sourceType = .photoLibrary
        present(picker, animated: true, completion: nil)
    }

    @IBAction func checkPressed(_ sender: Any) {
        //making checks before sending out request
        let title = bondName.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        if title.characters.count == 0{
            bondName.shake()
            return
        }
        if selectedContacts.count == 0{
            present(Globals.instance.showMessage(title: "Attention", message: "you need atleast one contact to create a bond with", actionHandler: nil), animated: true, completion: nil)
            return
        }

        //clear to proceed
        let activity = showActivityIndicator()
        let userInfo = UserDefaults.standard.value(forKey: "userInfo") as! [String:Any]
        let _id = userInfo["_id"] as! String
        selectedContacts.append(_id)
        Alamofire.request(APIEndPoints.createBond, method: .post, parameters: ["created_by":_id,"members":selectedContacts,"picture": self.url, "name":title], encoding: JSONEncoding.default, headers: nil).responseJSON{response in
            DispatchQueue.main.async {
                activity.animate = false
                 _=self.navigationController?.popViewController(animated: true)
            }
            guard let serverResponse = response.value as? [String:Any]
                else{
                    DispatchQueue.main.async {
                        self.present(Globals.instance.showMessage(title: "Error", message: "A network error was encountered", actionHandler: nil), animated: true, completion: nil)
                    }
                    return
            }
            guard let success = serverResponse["success"] as? Bool
                else{
                    DispatchQueue.main.async {
                        self.present(Globals.instance.showMessage(title: "Error", message: "An error was encountered", actionHandler: nil), animated: true, completion: nil)
                    }
                    return
                }
            if success{
                DispatchQueue.main.async {
                    self.present(Globals.instance.showMessage(title: "Success", message: "Your bond was created successfully!", actionHandler: nil), animated: true, completion: { 
                    })
                }
            }else{
                
            }
        }
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        bondName.resignFirstResponder()
    }

    func getContacts(){
        privateCtx.perform {
            self.contacts = Contact.getContacts(context: self.privateCtx)
        }

    }
    func uploadImageToServer(image: UIImage){
        if let formData = UIImageJPEGRepresentation(image, 0.7){
            let mimeType = "image/jpeg"
            let uploadUrl = URL(string : fileuploadUrl)!
            Alamofire.upload(multipartFormData: { (mpFormdata) in
                mpFormdata.append(formData, withName: "file",fileName: "yo.jpg", mimeType: mimeType)
            }, to: uploadUrl, encodingCompletion: { (result) in
                switch(result){
                case .success(let upload, _ ,_):
                    upload.uploadProgress(closure: {(progress) in
                        //handler this progress
                        print(progress.fractionCompleted)
                    })
                    upload.responseJSON(completionHandler: { (response) in
                        switch response.result{
                        case .success(let value):
                            let responseValue = value as? [String : Any]
                            if let url = responseValue?["Url"] as? String{
                                self.bondPicture.sd_setImage(with: URL(string: url)!)
                                self.url = url
                            }else{
                                DispatchQueue.main.async {
                                    self.present(Globals.instance.showMessage(title: "Attention", message: "There was an error uploading your picture, try again later!", actionHandler: nil), animated: true, completion: nil)
                                }
                            }
                            break
                        default:
                            DispatchQueue.main.async {
                                self.present(Globals.instance.showMessage(title: "Attention", message: "There was an error uploading your picture, try again later!", actionHandler: nil), animated: true, completion: nil)
                            }
                            break
                        }
                    })
                    break
                case .failure(let error):
                    let alert = Globals.instance.showMessage(title: "Attention", message: "Error uploading image file: \(error)", actionHandler: nil)
                    self.present(alert, animated: true, completion: nil)

                }
            })
        }

    }

}

extension NewBondVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerEditedImage] as! UIImage
        uploadImageToServer(image: chosenImage)
        dismiss(animated: true, completion: nil)
    }
}
extension NewBondVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if contacts != nil{
            return contacts!.count
        }
        return 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let contact = contacts![indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! NewBondCell
        cell.nameLabel.text = contact.name
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //finding id of selected contact
        var _id = ""
        privateCtx.performAndWait {
            _id = self.contacts![indexPath.row].id!
        }
        let cell = tableView.cellForRow(at: indexPath) as! NewBondCell
        let value = !cell.nameLabel.isSelected
        cell.nameLabel.isSelected = value
        if value{
            selectedContacts.append(_id)
        }else{
            selectedContacts.remove(at: selectedContacts.index(of: _id)!)
        }
    }
}

extension NewBondVC: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
