//
//  HomeVC.swift
//  FamBond.test
//
//  Created by Jatin Garg on 05/01/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import Foundation
import UIKit


class HomeVC: UIViewController, UITextFieldDelegate{
    
    lazy var numberTextField: FTextField = {
        let t = FTextField()
        t.delegate = self
        t.keyboardType = .numberPad
        t.placeholder = "Enter Phone Number"
        return t
    }()
    
    var countryCode: FTextField = {
        let t = FTextField()
        t.isEnabled = false
        t.text = "+91"
        return t
    }()
    
    var logoView: UIImageView = {
        let i = UIImageView()
        i.translatesAutoresizingMaskIntoConstraints = false
        i.image = #imageLiteral(resourceName: "icon")
        i.contentMode = .scaleAspectFit
        return i
    }()
    
    lazy var submitBtn: Fbutton = {
        let b = Fbutton()
        b.translatesAutoresizingMaskIntoConstraints = false
        b.setTitle("Submit", for: .normal)
        b.addTarget(self, action: #selector(submitPressed), for: .touchUpInside)
        return b
    }()
    
    var infoLabel: UILabel = {
        let l = UILabel()
        l.text = "Fambond will send an OTP to confirm your phone number.\n Carrier SMS charges may apply"
        l.adjustsFontSizeToFitWidth = true
        l.font = UIFont.systemFont(ofSize: 12)
        l.textColor = .white
        l.numberOfLines = 0
        l.textAlignment = .center
        return l
    }()
    
    var termsOfService: UIButton = {
        let b = UIButton(type: .system)
        b.setTitle("Terms Of Service", for: .normal)
        b.tintColor = .white
        b.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        return b
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        view.backgroundColor = Globals.instance.lightBlue
    }
    
    func setupViews(){
        view.addSubview(numberTextField)
        view.addSubview(countryCode)
        view.addSubview(logoView)
        view.addSubview(submitBtn)
        view.addSubview(infoLabel)
        view.addSubview(termsOfService)
        //Constraints
        logoView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        logoView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.5).isActive = true
        logoView.topAnchor.constraint(equalTo: view.topAnchor, constant: 50).isActive = true
        logoView.bottomAnchor.constraint(equalTo: numberTextField.topAnchor, constant: -50).isActive = true
        countryCode.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20).isActive = true
        numberTextField.leadingAnchor.constraint(equalTo: countryCode.trailingAnchor, constant: 10).isActive = true
        numberTextField.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        countryCode.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        countryCode.widthAnchor.constraint(equalToConstant: 50).isActive = true
        numberTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        submitBtn.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        submitBtn.anchor(numberTextField.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 40, leftConstant: 50, bottomConstant: 0, rightConstant: 50, widthConstant: 0, heightConstant: 60)
        
        infoLabel.anchorCenterXToSuperview()
        infoLabel.anchor(nil, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 20, bottomConstant: 50, rightConstant: 20, widthConstant: 0, heightConstant: 40)
        
        termsOfService.anchorCenterXToSuperview()
        termsOfService.anchor(infoLabel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 100, heightConstant: 0)
    }
    
    func submitPressed(){
        if numberTextField.text?.characters.count != 0{
            let number = countryCode.text! + numberTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let otp = OTPVC()
            otp.number = number
            present(otp, animated: true, completion: nil)
        }else{
            numberTextField.shake()
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        numberTextField.resignFirstResponder()
    }
    
    
}
