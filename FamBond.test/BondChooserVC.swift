//
//  BondChooserVC.swift
//  FamBond.test
//
//  Created by Jatin Garg on 22/02/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import UIKit
import CoreData

class BondChooserCell: UITableViewCell{

    @IBOutlet weak var bondLabel: PriorityLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        bondLabel.isSelected = false
    }

}

class BondChooserVC: UITableViewController {

    let pvtQueueContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)

    var selectedBonds = [Group_CD]()
    weak var delegate: NewPollVC? = nil
    var bonds: [Group_CD]? {
        didSet{
            if (bonds != nil){
                tableView.reloadData()
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        pvtQueueContext.parent = DBManager.getContext()
        pvtQueueContext.perform {
            self.bonds = Group_CD.returnAllGroups(context: self.pvtQueueContext)
        }

    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if bonds == nil{
            return 0
        }
        return bonds!.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! BondChooserCell
        cell.bondLabel.text = bonds![indexPath.row].name
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! BondChooserCell
        let value = !cell.bondLabel.isSelected
        cell.bondLabel.isSelected = value
        if value{
            //add to selections
            selectedBonds.append(bonds![indexPath.row])

        }else{
            //remove from selections
            selectedBonds.remove(at: selectedBonds.index(where: { (element) -> Bool in
                element.mongoid == bonds![indexPath.row].mongoid
            })!)
        }
    }

    @IBAction func donePressed(_ sender: Any) {
        if selectedBonds.count > 0{
            var b = [String]()
            selectedBonds.forEach{b.append($0.mongoid!)}
            delegate?.selectedBonds = b
            dismiss(animated: true, completion: nil)
        }else{
            present(Globals.instance.showMessage(title: "Attention", message: "You must select at least one bond", actionHandler: nil), animated: true, completion: nil)
        }
    }

    @IBAction func cancelPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

}
