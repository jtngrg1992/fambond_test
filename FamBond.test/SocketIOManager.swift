
//
//  SocketIOManager.swift
//  socket
//
//  Created by Jatin Garg on 21/12/16.
//  Copyright © 2016 Jatin Garg. All rights reserved.
//

import Foundation
import SocketIO
import UIKit

class SocketIOManager: NSObject{
    static let sharedInstace = SocketIOManager()
    override init(){
        super.init()
    }

    var socket = SocketIOClient(socketURL: URL(string: "http://52.221.229.53:8000/")!, config: [.forceNew(true)])
    
    func connect(completionHandler: @escaping (Bool)->Void){
        //getting updating message to show up in navigation bar
        let delegate = UIApplication.shared.delegate as! AppDelegate
        if let tabVc = delegate.window?.rootViewController as? CustomTabBarController{
            let navigationVC = tabVc.viewControllers?.first as? UINavigationController
            navigationVC?.showOngoingActivity()
        }
        socket.connect(timeoutAfter: 10, withHandler: {
            completionHandler(false)
        })
        socket.once("clientSocket",callback:{(data,ack)->Void in
            //logging
            if let userInfo = UserDefaults.standard.value(forKey: "userInfo") as? [String:Any]{
                let phone = userInfo["phone"] as! String
                self.connectWithPhone(phone: phone){_ in
                    print("Logged")
                    completionHandler(true)
                }
            }

        })}

    func disconnect(){
        socket.disconnect()
    }

    func connectWithPhone(phone: String,completionHandler : @escaping (_ userList: [String : Any]) -> Void){
        print("Emitting login signal")
        socket.emit("connectUser", phone)
        socket.once("userList", callback: {(dataArray, ack) -> Void in
            completionHandler(dataArray[0] as! [String : Any])
        })
        
        listenForOtherMessages()
    }
    

    func sendMessageToGroup(message: [String:Any]){
        //checking for connection
        print("Socket connection Status: \(socket.status.rawValue)")
        if socket.status.rawValue != 3{ //disconnected
            print("Attempting to reconnect")
            self.connect {_ in 
                //nothing
            }
        }
       
        socket.emit("sendGroupMessage", message)

        
    }
    func getMessageFromGroup(completionHandler: @escaping (_ messageInfo : [String : Any]) -> Void){
        socket.on("groupMessage", callback: {(msg,ack) -> Void in
            completionHandler(msg[0] as! [String : Any])
        })
    }

    func joinAllRooms(){
        if let userInfo = UserDefaults.standard.value(forKey: "userInfo") as? [String:Any]{
            let id = userInfo["_id"] as! String
            socket.emit("joinRoom", ["_id":id])
        }
    }
    func sendStartTypingMessage(groupInfo: [String : Any]) {
        if let userInfo = UserDefaults.standard.value(forKey: "userInfo") as? [String : Any]{
            socket.emit("startType", userInfo,groupInfo)
        }
        
    }
    
    func sendStopTypingMessage(groupInfo: [String : Any]) {
        if let userInfo = UserDefaults.standard.value(forKey: "userInfo") as? [String : Any]{
            socket.emit("stopType", userInfo,groupInfo)
        }
        
    }
    
    func listenForOtherMessages(){
        socket.on("userTypingUpdate", callback: {(data, socketAck) -> Void in
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userTypingNotification"), object: data[0] as! [String : Any])
        })
        
        socket.on("userStopTypingUpdate", callback: {(data, socketAck) -> Void in
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userTypingStoppedNotification"), object: data[0] as! [String : Any])
        })
    }

    func notifyRead(messageID: String){
        guard let userInfo = UserDefaults.standard.value(forKey: "userInfo") as? [String:Any] else {return}
        let infoToSend = ["messageID": messageID, "userInfo": userInfo] as [String : Any]
        socket.emit("readMessage", infoToSend)
    }
    
    
    
}
