//
//  Photo.swift
//  FamBond.test
//
//  Created by Jatin Garg on 02/03/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class Photo: NSObject{
    var url: URL
    var messageID: String!
    var bondId: String!//for dispatching message once the upload is complete
    var bondName: String!//for dispatching message once the upload is complete
    dynamic var image: UIImage? = nil
    dynamic var photoDownload: MediaDownload?
    dynamic var photoUpload: MediaUpload?

    var isDownloading = false
    var isUploading = false
    init(url: URL){
        self.url = url
    }

    init(image: UIImage){
        self.url = NSURL() as URL
        self.image = image
    }

    func startDownload() -> Progress{
        let newDownload = MediaDownload(url: self.url)
        isDownloading = true
        newDownload.completionHandler = {imageData,error in
            if let imgData = imageData{
                //update coredata
                let pvtContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
                pvtContext.parent = DBManager.getContext()
                pvtContext.perform {
                    let msg = Message_CD.returnMessage(withId: self.messageID, inContext: pvtContext)
                    let mediaDirectory = getMediaUrl()
                    let fileURL = mediaDirectory?.appendingPathComponent("\(NSUUID().uuidString).jpg")
                    print("downloaded to \(fileURL)")
                    guard let _ = (try? imgData.write(to: fileURL!, options: .atomic))else{return}
                    self.image = UIImage(data: imgData) // for calculating dimensions
                    msg?.thumbUrl = fileURL?.lastPathComponent
                    msg?.mediaUrl = fileURL?.lastPathComponent
                    msg?.media_width = Float((self.image?.size.width)!)
                    msg?.media_height = Float((self.image?.size.height)!)
                    msg?.isDelivered = true
                    msg?.isProcessed = true
                    try? pvtContext.save()
                    DBManager.getContext().performAndWait {
                        try? DBManager.getContext().save()
                    }
                    self.isDownloading = false

                }
            }else{
                self.reportError(error: error)
            }
            self.photoDownload = nil
        }
        newDownload.startDownload()
        self.photoDownload = newDownload
        return newDownload.progress
    }

    func startUpload(){
        guard let imageData = UIImageJPEGRepresentation(image!, 0.5) else {return}
        let newUpload = MediaUpload(data: imageData, uploadURL: URL(string: fileuploadUrl)!)
        isUploading = true
        newUpload.completionHandler = {url,error in
            self.photoUpload = nil
            if let uploadUrl = url{
                print("upload success :\(uploadUrl)")
                //updating coredata
                DBManager.getContext().performAndWait {
                    let msg = Message_CD.returnMessage(withId: self.messageID, inContext: DBManager.getContext())
                    msg?.isProcessed = true
                    msg?.text = uploadUrl
                    try? DBManager.getContext().save()
                }

                self.isUploading = false
                //sending message
                let sender = MessageSender(type: "image", text: nil, localMediaURL: nil, remoteMediaURL: uploadUrl, thumbURL: nil, bondID: self.bondId, bondName: self.bondName, thumb: self.image)
                sender.shouldSendToServer = true
                sender.shouldAddToDB = false
                sender.setMessageID(id: self.messageID)
                sender.send()

            }else{
                self.reportError(error: error)
                return
            }
        }
        newUpload.startUpload(withExtension: "jpg")
        self.photoUpload = newUpload
        
    }
    
    private func reportError(error: Error?){
        print(error?.localizedDescription ?? "Error reported from photo downloader")
    }
}
