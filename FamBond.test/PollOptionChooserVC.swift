//
//  PollOptionChooserVC.swift
//  FamBond.test
//
//  Created by Jatin Garg on 22/02/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import UIKit
import BEMCheckBox
import Alamofire

class PollOptionChooserCell: UITableViewCell{


    @IBOutlet weak var optionLabel: PriorityLabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        optionLabel.layer.cornerRadius = 0
        optionLabel.isSelected = false
    }
}

class PollOptionChooserVC: UITableViewController {

    var poll: [String:Any]? = nil
    var selfID: String? = nil
    var selectedOption: String? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        let doneBtn = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(donePressed))
        navigationItem.rightBarButtonItem = doneBtn
    }


    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if poll == nil{
            return 0
        }
        return (poll!["options"] as! [[String:Any]]).count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! PollOptionChooserCell
        let option = (poll!["options"] as! [[String:Any]])[indexPath.row]
        cell.optionLabel.text = (option["name"] as! String)
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //removing highlighting
        for subview in tableView.subviews{
            for sub in subview.subviews{
                if let cell = sub as? PollOptionChooserCell{
                    cell.optionLabel.isSelected = false
                }
            }
        }
        let cell = tableView.cellForRow(at: indexPath) as! PollOptionChooserCell
        cell.optionLabel.isSelected = true
        selectedOption = (poll?["options"] as! [[String:Any]])[indexPath.row]["name"] as? String
    }

    func donePressed(){
        if let option = selectedOption{
            let activity = showActivityIndicator()
            //sending poll response
            Alamofire.request(APIEndPoints.sendPollResponse, method: .post, parameters: ["user_id":selfID!,"poll_id":poll!["_id"] as! String,"option":option], encoding: JSONEncoding.default, headers: nil).responseJSON{response in
                DispatchQueue.main.async {
                    activity.animate = false
                   _=self.navigationController?.popViewController(animated: true)
                }

                guard let serverResponse = response.value as? [String:Any] else{
                    DispatchQueue.main.async {
                        self.present(Globals.instance.showMessage(title: "Attention", message: "Some error was encountered, try again later", actionHandler: nil), animated: true, completion: nil)
                    }
                    return
                }
                guard let success = serverResponse["success"] as? Bool else{
                    DispatchQueue.main.async {
                        self.present(Globals.instance.showMessage(title: "Attention", message: "Some error was encountered, try again later", actionHandler: nil), animated: true, completion: nil)
                    }
                    return
                }
                if success{
                    DispatchQueue.main.async {
                        self.present(Globals.instance.showMessage(title: "Success", message: "Your response was recorded successfully!", actionHandler: nil), animated: true, completion: nil)
                    }
                }else{
                    let error = serverResponse["errors"] as! String
                    DispatchQueue.main.async {
                        self.present(Globals.instance.showMessage(title: "Attention", message: "\(error)", actionHandler: nil), animated: true, completion: nil)
                    }
                }
            }
        }
        }

}
