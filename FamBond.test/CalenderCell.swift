//
//  CalenderCell.swift
//  FamBond.test
//
//  Created by Jatin Garg on 13/02/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import Foundation
import JTAppleCalendar

class CalendarCell: JTAppleDayCellView{
    
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var marker: UIView!

}
