//
//  AppDelegate.swift
//  FamBond.test
//
//  Created by Jatin Garg on 05/01/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import UIKit
import CoreData
import UserNotifications
import Alamofire

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var customNVC: UINavigationController!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.makeKeyAndVisible()
        let home = HomeVC()
        let sb = UIStoryboard(name: "Registration", bundle: nil)
        let personal = sb.instantiateViewController(withIdentifier: "personal") 
        if (UserDefaults.standard.value(forKey: "userInfo") as? [String:Any]) != nil{
            let tabVC = CustomTabBarController()
            
            self.window?.rootViewController = tabVC
        }else{
            self.window?.rootViewController = home
        }
        Globals.instance.createSubDirInDocs(subDir: "media")
        UNUserNotificationCenter.current().requestAuthorization(options: [.sound,.alert,.badge]){success,error in
            //nil
            if success{
                application.registerForRemoteNotifications()
            }else{
                print("permission was not granted for push notifications")

            }
            
        }

        return true
        
    }


    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("success ful , device token is : \(tokenString(deviceToken)))")
        //send this token to server for use
        if let userInfo = UserDefaults.standard.value(forKey: "userInfo") as? [String:Any]{
            Alamofire.request(APIEndPoints.sendDeviceToken, method: .post, parameters: ["_id": userInfo["_id"] as! String,"deviceID":tokenString(deviceToken)], encoding: JSONEncoding.default, headers: nil).responseJSON{response in
                guard let serverResponse = response.value as? [String:Any] else{print("error obtainting server response");return}
                guard let success = serverResponse["success"] as? Bool else{print("error obtaining success parameter"); return}
                if success{
                    print("set device token")
                }else{
                    print("failure setting device token")
                }
             }
        }

    }

    func tokenString(_ deviceToken:Data) -> String{
        //code to make a token string
        let bytes = [UInt8](deviceToken)
        var token = ""
        for byte in bytes{
            token += String(format: "%02x",byte)
        }
        return token
    }

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("failure in registering : \(error)")
    }


    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        print("Did enter background")
        SocketIOManager.sharedInstace.disconnect()
        print("socket disconnected")
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        //SocketIOManager.sharedInstace.connect(completionHandler: {()->Void in })
         application.applicationIconBadgeNumber = 0
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        getOfflineMessages()
        if SocketIOManager.sharedInstace.socket.status.rawValue != 3{
            //disconnected
            SocketIOManager.sharedInstace.connect {success in
                if success{
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "connectionEstablishedNotification"),object: nil)

                }else{
                    DispatchQueue.main.async {
                        self.window?.rootViewController?.present(Globals.instance.showMessage(title: "Attention!", message: "Looks like you are not connected. Please check your connectivity", actionHandler: nil), animated: true, completion: nil)
                    }
                }
            }
        }
        
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        SocketIOManager.sharedInstace.disconnect()
        DBManager.saveContext()
    }

}
