//
//  MessageSender.swift
//  FamBond.test
//
//  Created by Jatin Garg on 07/03/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import UIKit

class MessageSender: NSObject{
    var type: String?
    var text: String?
    var localMediaURL: String?
    var remoteMediaURL: String?
    var thumbURL: String?
    var bondID: String?
    var bondName: String?
    var shouldSendToServer: Bool = false
    var shouldAddToDB: Bool = false
    var thumbNail: UIImage?
    var messageID: String?
    var mediaHeight: Float?
    var mediaWidth: Float?

    init(type: String?,text: String?,localMediaURL: String?, remoteMediaURL: String?, thumbURL: String?, bondID: String?, bondName: String?, thumb: UIImage?){
        self.type = type
        self.text = text
        self.localMediaURL = localMediaURL
        self.remoteMediaURL = remoteMediaURL
        self.thumbURL = thumbURL
        self.bondID = bondID
        self.bondName = bondName
        self.thumbNail = thumb
    }

    func send(){
        //constructing message dictionary
        let userInfo = UserDefaults.standard.value(forKey: "userInfo") as! [String:Any]
        let sender = ["name":userInfo["firstName"] as! String, "_id": userInfo["_id"] as! String, "phone": userInfo["phone"] as! String]

        var message = ["id": NSUUID().uuidString,"isDelivered": false,"sender": sender,"bondid": bondID!,"bondname": bondName!,"type": type!] as [String : Any]
        //setting optional properties
        message["thumbUrl"] = thumbURL
        message["mediaUrl"] = localMediaURL


        if let height = thumbNail?.size.height, let width = thumbNail?.size.width{
            message["mediaWidth"] = Float(width)
            message["mediaHeight"] = Float(height)
        }
        
        if let height = mediaHeight, let width = mediaWidth{
            message["mediaWidth"] = width
            message["mediaHeight"] = height
        }


        if shouldAddToDB{
            //adding db specific fields
            message["isSender"] = true
            message["time"] = Date()
            message["mediaUrl"] = localMediaURL
            message["thumbUrl"] = thumbURL
            message["text"] = text

            //making insertion using main context
            Message_CD.insertMessage(withInfo: message, context: DBManager.getContext())

        }

        if shouldSendToServer{
            switch type!{
            case "image","video" : message["text"] = remoteMediaURL
            case "text": message["text"] = text
            default : break
            }
            message["isSender"] = nil
            message["time"] = nil
            message["mediaUrl"] = nil
            message["thumbUrl"] = nil
            if messageID != nil{
                message["id"] = self.messageID!
            }
            SocketIOManager.sharedInstace.sendMessageToGroup(message: message)
        }

    }

    func parseUnicode(from text: String) -> NSMutableString{
        let convertedString = text.mutableCopy() as! NSMutableString
        let transform = "Any-Hex/Java"
        CFStringTransform(convertedString, nil, transform as NSString, true)
        return convertedString
    }

    func setMessageID(id: String){
        messageID = id
    }

    func setMediaDimentions(width: Float, height: Float){
        mediaWidth = width
        mediaHeight = height
    }


}
