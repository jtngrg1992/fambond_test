//
//  ContactsChooserVC.swift
//  FamBond.test
//
//  Created by Jatin Garg on 23/02/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import UIKit
import CoreData

class ContactsChooserCell: UITableViewCell{

    @IBOutlet weak var nameLabel: PriorityLabel!
    @IBOutlet weak var phoneLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        nameLabel.isSelected = false
        nameLabel.layer.cornerRadius = 0
    }

}
class ContactsChooserVC: UITableViewController {

    var contacts: [Contact]?{
        didSet{
            tableView.reloadData()

        }
    }

    let pvtContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)

    override func viewDidLoad() {
        super.viewDidLoad()
        pvtContext.parent = DBManager.getContext()
        pvtContext.perform {
            self.contacts = Contact.getContacts(context: self.pvtContext)
        }
        navigationItem.title = "Who to assign"
        let done = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(donePressed))
        navigationItem.rightBarButtonItem = done
    }

    weak var delegate: TaskCreationVC? = nil
    weak var delegate2: GroupInfoVC? = nil
    func donePressed(){
        if selectedContacts.count > 0{
            delegate?.selectedContacts = self.selectedContacts
            _=navigationController?.popViewController(animated: true)
        }

    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if contacts == nil{
            return 0
        }
        return contacts!.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var contact = Contact()
        pvtContext.performAndWait {
            contact = self.contacts![indexPath.row]
        }

        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ContactsChooserCell
        cell.nameLabel.text = contact.name
        cell.phoneLabel.text = contact.phone
        return cell
    }

    var selectedContacts = [String]()

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! ContactsChooserCell
        let value = !cell.nameLabel.isSelected
        var _id = ""
        pvtContext.performAndWait {
            _id = self.contacts![indexPath.row].id!
        }
        if value{
            //start appending
            selectedContacts.append(_id)
        }else{
            //start popping
            selectedContacts.remove(at: selectedContacts.index(of: _id)!)
        }
        cell.nameLabel.isSelected = value
    }

}
