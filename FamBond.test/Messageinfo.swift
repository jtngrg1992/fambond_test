//
//  Messageinfo.swift
//  FamBond.test
//
//  Created by Jatin Garg on 08/03/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import UIKit

class MessageInfoVC: UITableViewController{


    @IBOutlet var bubbleTrailingConstraint: NSLayoutConstraint!
    @IBOutlet var bubbleLeadingConstraint: NSLayoutConstraint!
    @IBOutlet var bubbleWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var messageTextView: UILabel!

    var msgToDisplay: Message_CD?{
        didSet{
            updateUI()
            //make restAPI call
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
    }

    private func updateUI(){
        //setting alignment
        let bubbleFrame = getEstimatedFrame()
        bubbleLeadingConstraint?.isActive = false
        bubbleWidthConstraint?.constant = bubbleFrame.width + 30
        tableView.tableHeaderView?.frame.size.height = bubbleFrame.height + 45

        //setting content
        if msgToDisplay?.type! == "text"{
            messageTextView.text = msgToDisplay?.text!
        }

    }

    private func getEstimatedFrame()->CGRect{
        //getting the message text on main queue to avoid hassle
        let text = msgToDisplay!.text!
        let size = CGSize(width: maxBubbleWidth, height: 1000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        let estimatedFrame = NSString(string: text).boundingRect(with: size, options: options, attributes:[NSFontAttributeName: UIFont.systemFont(ofSize: messageTextSize) ], context: nil)
        return estimatedFrame
    }
}


//MARK: - Tableview delegates
extension MessageInfoVC{

}
