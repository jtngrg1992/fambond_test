//
//  AppHomeVC.swift
//  FamBond.test
//
//  Created by Jatin Garg on 09/01/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import UIKit
import CoreData
import Alamofire
import UserNotifications

let cellSize: CGFloat = 80
let titlefontSize: CGFloat = 14
let subTitleFontSize: CGFloat = 12
let imgViewSize: CGFloat = 60


//MARK:- home VC After the app is done initializing*********************************************************************

class AppHomeVC: UIViewController, UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate, UNUserNotificationCenterDelegate{
    
    var activeGroupID: String? = nil // for recording the active group id
    
    lazy var addBtn: UIButton = {
        let b = UIButton(type: .system)
        b.setTitle("+", for: .normal)
        b.titleLabel?.font = UIFont.boldSystemFont(ofSize: 30)
        b.backgroundColor = Globals.instance.secondColor
        b.layer.cornerRadius = 35
        b.layer.masksToBounds = true
        b.tintColor = .white
        b.addTarget(self, action: #selector(btnPressed), for: .touchUpInside)
        return b
    }()
    
    let dispatchQ = DispatchQueue.global(qos: .userInitiated)
    
    var fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>!
    
    lazy var tableView: UITableView = {
        let t = UITableView()
        t.tableFooterView = UIView()
        t.register(BasicCell.self, forCellReuseIdentifier: "cell")
        t.backgroundColor = Globals.instance.lightBlue
        t.dataSource = self
        t.delegate = self
        t.separatorStyle = .none
        return t
    }()
    
    let pvtQueueContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)

    var groups = [[String : Any]]()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupSubviews()
        setNavigationItems()
        UNUserNotificationCenter.current().delegate = self
        view.backgroundColor = Globals.instance.lightBlue
        let fetchReq = NSFetchRequest<NSFetchRequestResult>(entityName: "Group_CD")
        fetchReq.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
        fetchReq.fetchBatchSize = 10
        pvtQueueContext.parent = DBManager.getContext()
        fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchReq, managedObjectContext: DBManager.getContext(), sectionNameKeyPath: nil, cacheName: nil)
        fetchedResultsController.delegate = self
        do{
            try fetchedResultsController.performFetch()
        }catch let error{
            print("CD Fetch error: \(error)")
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        activeGroupID = nil
        tabBarController?.tabBar.isHidden = false
        NotificationCenter.default.addObserver(self, selector: #selector(connectionEstablished), name: Notification.Name(rawValue: "connectionEstablishedNotification"), object: nil)
        connectionEstablished()
        askForBonds()
        SocketIOManager.sharedInstace.joinAllRooms()
    }

    
    func askForBonds(){
        let userInfo = UserDefaults.standard.value(forKey: "userInfo") as! [String:Any]
        let _id = userInfo["_id"] as! String
        print("[INFO] Asking for bonds")
        Alamofire.request(APIEndPoints.getBonds, method: .post, parameters: ["_id":_id], encoding: JSONEncoding.default, headers: nil).responseJSON{response in
            let serverResponse = response.value as? [String:Any]
            guard let success = serverResponse?["success"] as? Bool else{return}
            if success{
                let data = serverResponse?["data"] as! [[String : Any]]
                for bondInfo in data{
                    self.pvtQueueContext.perform {
                        Group_CD.createBondUsingData(data: bondInfo, context: self.pvtQueueContext)
                    }
                }
                self.pvtQueueContext.perform {
                    try? self.pvtQueueContext.save()
                    DBManager.getContext().performAndWait {
                        try? DBManager.getContext().save()
                    }

                }

            }else{
                DispatchQueue.main.async {
                    self.present(Globals.instance.showMessage(title: "Attention", message: "An error was encountered, please try again later", actionHandler: nil), animated: true, completion: nil)

                }
            }

        }

    }

    func setNavigationItems(){
        let left = UIBarButtonItem(image: #imageLiteral(resourceName: "menu-button"), style: .plain, target: self, action: #selector(menuBtnPressed))
        navigationItem.leftBarButtonItem = left
        let right = UIBarButtonItem(title: "SOS", style: .plain, target: self, action: #selector(sosPressed))
        navigationItem.rightBarButtonItem = right
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    func sosPressed(){
        pvtQueueContext.performAndWait {
            //check if any sos contacts
            guard let contacts = SOSContact.returnSOSContacts(withContext: self.pvtQueueContext) else{return}
            if contacts.count != 0{
                var contactNames = ""
                contacts.forEach{
                    contactNames = "\(contactNames) \($0.name!)"
                }
                DispatchQueue.main.async {
                    self.present(Globals.instance.showMessage(title: "Attention", message: "An SOS message has been dispatched to \(contactNames)", actionHandler: nil), animated: true, completion: nil)
                }
            }else{
                //chose sos contacts
                DispatchQueue.main.async {
                    let sb = UIStoryboard(name: "Main", bundle: nil)
                    let sosVC = sb.instantiateViewController(withIdentifier: "sosContacts") as! SOSContactsVC
                    sosVC.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(sosVC, animated: true)
                }
            }
        }
    }


    func menuBtnPressed(){
        tabBarController?.tabBar.isHidden = true
        //presenting dashboard here
        let dashboard = UIStoryboard(name: "Storyboard", bundle: .main).instantiateViewController(withIdentifier: "dashboard") as! UINavigationController
        self.present(dashboard, animated: true, completion: nil)
    }

    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        if type == .insert{
            tableView.insertRows(at: [newIndexPath!], with: .automatic)
            print("rows inserted")
        }
        if type == .update{
            tableView.reloadRows(at: [newIndexPath!], with: .automatic)
        }
        
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
        tableView.reloadData()
        
    }
    func connectionEstablished(){
        print("Connection noti received'")
        self.navigationController?.hideActivityIndicator(with: "Bonds")
        SocketIOManager.sharedInstace.socket.off("groupMessage")
        listenToIncomingMessages()
        SocketIOManager.sharedInstace.joinAllRooms()
    }

    func listenToIncomingMessages(){
        // listening to group message
        SocketIOManager.sharedInstace.getMessageFromGroup { [unowned self] (msg) in
            //deciding if self message or not
            print("Got Message from server")
            let message = msg["message"] as! [String:Any]
            print(message)
            //checking if self message
            self.pvtQueueContext.perform {
                if let m = Message_CD.returnMessage(withId: message["id"] as! String, inContext: self.pvtQueueContext){
                    //message was found in local db, update delivery status and save
                    m.isDelivered = true
                    try? self.pvtQueueContext.save()
                    try? DBManager.getContext().save()
                }else{
                    //this is a new message, save in local db
                    var info = message
                    info["time"] = Date();info["isDelivered"] = true
                    Message_CD.insertMessage(withInfo: info, context: self.pvtQueueContext)
                    try? self.pvtQueueContext.save()
                    try? DBManager.getContext().save()
                    self.presentLocalNotification(forMessage: message)
                }
            }
        }
    }

    func presentLocalNotification(forMessage message: [String:Any]){
        guard let messagGroupID = message["bondid"] as? String else{return}
        let content = UNMutableNotificationContent()
        content.title = message["bondname"] as! String
        //determinging the notification body
        let messageType = message["type"] as! String
        var notificationBody = ""
        switch messageType {
        case "text","botMessage":notificationBody = message["text"] as! String
        case "image": notificationBody = "Image"
        case "video": notificationBody = "video"
        default:break
        }
        content.body = notificationBody
        content.sound = UNNotificationSound.default()
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 0.1, repeats: false)
        let request = UNNotificationRequest(identifier: "message", content: content, trigger: trigger)
        if activeGroupID != nil {
            //check if message is received in some other group
            if activeGroupID != messagGroupID{
                //need to present notification
                UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
                incrementUnreadMsgCounter(groupID: messagGroupID)
            }
        }else{
            UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
            incrementUnreadMsgCounter(groupID: messagGroupID)
        }
    }

    func incrementUnreadMsgCounter(groupID: String){
        pvtQueueContext.perform {
            Group_CD.incrementUnreadCounter(id: groupID, context: self.pvtQueueContext)
            try? self.pvtQueueContext.save()
            DBManager.getContext().perform {
                try? DBManager.getContext().save()
            }
        }
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.sound,.badge,.alert])
    }
    func parseUnicode(from text: String) -> NSMutableString{
        let convertedString = text.mutableCopy() as! NSMutableString
        let transform = "Any-Hex/Java"
        CFStringTransform(convertedString, nil, transform as NSString, true)
        return convertedString
    }


    func btnPressed(sender: UIButton){
        switch sender {
        case addBtn:
            let sb = UIStoryboard(name: "Main", bundle: nil)
            let bondCreateVC = sb.instantiateViewController(withIdentifier: "bondcreate")
            navigationController?.pushViewController(bondCreateVC, animated: true)
            break
        default : break
        }
    }
    
    func setupSubviews(){
        view.addSubview(tableView)
        view.insertSubview(addBtn, aboveSubview: tableView)
        
        addBtn.anchor(nil, left: nil, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 50, rightConstant: 50, widthConstant: 70, heightConstant: 70)
        tableView.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return groups.count
        if let count = fetchedResultsController.sections?[0].numberOfObjects{
            return count
        }
        return 0
    }
    
    func findLastMessageForGroup(group: Group_CD) -> Message_CD?{
        var message: Message_CD? = nil
        group.managedObjectContext?.performAndWait {
            let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Message_CD")
            fetch.predicate = NSPredicate(format: "group.mongoid = %@", group.mongoid!)
            fetch.sortDescriptors = [NSSortDescriptor(key: "time", ascending: false)]
            fetch.fetchLimit = 1
            let messages = (try? group.managedObjectContext?.fetch(fetch)) as? [Message_CD]
            message = messages?.first
        }
        return message
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! BasicCell
        let group = fetchedResultsController.object(at: indexPath) as! Group_CD
        var groupName = ""
        var picture: String? = nil
        var unreadCount: Int16 = 0
        group.managedObjectContext?.performAndWait {
            groupName = group.name!
            picture = group.picture
            unreadCount = group.unreadCount
        }
        cell.titleLabel.text = groupName
        if let message = findLastMessageForGroup(group: group){
            cell.subTitleLabel.text = message.text
            cell.timeLabel.text = formatTime(time: message.time)
        }
        if picture != nil{
            if let url = URL(string: picture!){
                cell.iView.sd_setImage(with: url)
            }
        }else{
            cell.iView.image = #imageLiteral(resourceName: "placeholder")
        }
        if unreadCount == 0{
            cell.badgeLabel.isHidden = true
        }else{
            cell.badgeLabel.isHidden = false
            cell.badgeLabel.text = "\(unreadCount)"
        }
        
        return cell
    }
    
    
    func formatTime(time: NSDate?) -> String?{
        if let time  = time{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "h:mm a"
            let time = dateFormatter.string(from: time as Date)
            return time
        }
        return nil
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sb = UIStoryboard(name: "Chatlog", bundle: nil)
        let chatlog = sb.instantiateViewController(withIdentifier: "chatlog") as! ChatlogV2VC
        var group: Group_CD? = nil
        fetchedResultsController.managedObjectContext.performAndWait {
            group = self.fetchedResultsController.object(at: indexPath) as? Group_CD
        }
        
        if group != nil{
            activeGroupID = group!.mongoid
            chatlog.group = group!
            chatlog.hidesBottomBarWhenPushed = true
            //reset unread counter
            group?.managedObjectContext?.perform {
                group?.unreadCount = 0
                try? group?.managedObjectContext?.save()
            }
            self.navigationController?.pushViewController(chatlog, animated: true)
        }
        
    }

    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let more = UITableViewRowAction(style: .normal, title: "More") { action, index in
            print("more button tapped")
        }
        more.backgroundColor = .lightGray

        let favorite = UITableViewRowAction(style: .normal, title: "Favorite") { action, index in
            print("favorite button tapped")
        }
        favorite.backgroundColor = .orange

        let share = UITableViewRowAction(style: .normal, title: "Share") { action, index in
            print("share button tapped")
        }
        share.backgroundColor = .blue
        
        return [share, favorite, more]
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
}

class BasicCell: UITableViewCell{
    var iView: UIImageView = {
        let i = UIImageView()
        i.image = #imageLiteral(resourceName: "placeholder")
        i.contentMode = .scaleAspectFill
        i.layer.cornerRadius = imgViewSize/2
        i.layer.masksToBounds = true
        return i
    }()
    
    var cardView: UIView = {
        let v = UIView()
        v.layer.shadowColor = UIColor.black.withAlphaComponent(0.7).cgColor
        v.layer.shadowOffset = CGSize(width: 0, height: 0)
        v.layer.shadowOpacity = 0.4
        v.layer.masksToBounds = false
        v.layer.shadowRadius = 1
        v.backgroundColor = Globals.instance.darkBlue
        return v
    }()
    
    var titleLabel: UILabel = {
        let l = UILabel()
        l.font = UIFont.boldSystemFont(ofSize: titlefontSize)
        l.textColor = .white
        l.text = "Title"
        return l
    }()
    
    var subTitleLabel: UILabel = {
        let l = UILabel()
        l.font = UIFont.systemFont(ofSize: subTitleFontSize)
        l.textColor = .white
        l.text = "You were added.."
        return l
    }()

    var badgeLabel: UILabel = {
        let l = UILabel()
        l.font = UIFont.systemFont(ofSize: 12)
        l.layer.cornerRadius = 10
        l.backgroundColor = Globals.instance.secondColor
        l.textColor = .white
        l.text = "1"
        l.textAlignment = .center
        l.layer.masksToBounds = true
        return l
    }()

    var timeLabel: UILabel = {
        let l = UILabel()
        l.font = UIFont.systemFont(ofSize: 12)
        l.textColor = .white
        l.text = ""
        l.textAlignment = .right
        return l
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    func setup(){
        accessoryType = .disclosureIndicator
        selectionStyle = .none
        backgroundColor = Globals.instance.lightBlue
        insertSubview(cardView, belowSubview: contentView)
        contentView.addSubview(iView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(subTitleLabel)
        contentView.addSubview(timeLabel)
        contentView.addSubview(badgeLabel)
        
        cardView.anchor(topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 4, leftConstant: 0, bottomConstant: 2, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        badgeLabel.anchorCenterYToSuperview()
        badgeLabel.anchor(nil, left: nil, bottom: nil, right: contentView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 10, widthConstant: 20, heightConstant: 20)
        iView.anchorCenterYToSuperview()
        iView.anchor(nil, left: contentView.leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 20, bottomConstant: 0, rightConstant: 0, widthConstant: imgViewSize, heightConstant: imgViewSize)
        titleLabel.anchorCenterYToSuperview()
        titleLabel.anchor(nil, left: iView.rightAnchor, bottom: nil, right: badgeLabel.leftAnchor, topConstant: 0, leftConstant: 20, bottomConstant: 0, rightConstant: 40, widthConstant: 0, heightConstant: 0)
        subTitleLabel.anchor(titleLabel.bottomAnchor, left: titleLabel.leftAnchor, bottom: nil, right: titleLabel.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        timeLabel.anchor(contentView.topAnchor, left: nil, bottom: nil, right: contentView.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 10, widthConstant: 60, heightConstant: 0)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        timeLabel.text = ""
        titleLabel.text = ""
        subTitleLabel.text = ""
    }
}
