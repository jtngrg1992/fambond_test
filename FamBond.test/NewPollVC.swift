//
//  NewPollVC.swift
//  FamBond.test
//
//  Created by Jatin Garg on 07/02/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import UIKit
import CoreData
import Alamofire

class NewPollVC: UIViewController {
    
    var groupID: Int? = nil
    
    let pvtQueueContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
    lazy var subjectTextField: UITextField = {
        let t = UITextField()
        let image = Globals.instance.getGraident()
        t.background = image
        t.layer.cornerRadius = 10
        t.layer.masksToBounds = true
        t.setAttributedPlaceholder(string: "Enter poll subject")
        t.textColor = .white
        return t
    }()
    
    var poll: Poll? = nil
    
    lazy var addBonds: UIButton = {
        let t = UIButton(type: .system)
        t.setTitle("Add Bonds", for: .normal)
        t.addTarget(self, action: #selector(addBondsPressed), for: .touchUpInside)
        t.layer.borderWidth = 2
        t.layer.borderColor = UIColor.white.cgColor
        t.layer.masksToBounds = true
        t.layer.cornerRadius = 4
        t.tintColor = .white
        return t
    }()
    
    lazy var optionsView: OptionsView = {
        let o = OptionsView()
        o.layer.masksToBounds = true
        o.layer.cornerRadius = 10
        o.backgroundColor = Globals.instance.darkBlue
        o.translatesAutoresizingMaskIntoConstraints = false
        o.delegeta = self
        return o
    }()
    
    lazy var submitBtn: UIButton = {
        let b = UIButton(type: .system)
        b.backgroundColor = Globals.instance.secondColor
        b.layer.masksToBounds = true
        b.layer.cornerRadius = 25
        b.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
        b.tintColor = .white
        b.addTarget(self, action: #selector(submitPressed), for: .touchUpInside)
        return b
    }()
    
    func submitPressed(){
        //getting title 
        let title = subjectTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        if title!.isEmpty{
            present(Globals.instance.showMessage(title: "Attention", message: "Poll title is required", actionHandler: nil), animated: true, completion: nil)
            return
        }
        if selectedBonds.count == 0{
            present(Globals.instance.showMessage(title: "Attention", message: "At least one bond is required to be sleected", actionHandler: nil), animated: true, completion: nil)
            return
        }

        //getting options
        var options = [[String:Any]]()
        optionsView.stack.arrangedSubviews.forEach { (subView) in
            if let tf = subView as? UITextField{
                if !(tf.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)!{
                    options.append(["name":(tf.text?.trimmingCharacters(in: .whitespacesAndNewlines))!,"count":0])
                }
            }
        }
        if options.count < 2{
            present(Globals.instance.showMessage(title: "Attention", message: "Atlest two options are required", actionHandler: nil), animated: true, completion: nil)
            return
        }
        let userinfo = UserDefaults.standard.value(forKey: "userInfo") as! [String:Any]
        let _id = userinfo["_id"] as! String
        let parameters = ["created_by": _id,"title":title!, "options": options,"bonds":selectedBonds] as [String : Any]
        let indeicator = self.showActivityIndicator()
        Alamofire.request(APIEndPoints.createPoll, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON{response in
            DispatchQueue.main.async {
                indeicator.animate = false
            }
            guard let serverResponse = response.value as? [String:Any] else{
                DispatchQueue.main.async {
                    self.present(Globals.instance.showMessage(title: "Attention", message: "An error was encountered, please try again later", actionHandler: nil), animated: true, completion: nil)
                    _=self.navigationController?.popViewController(animated: true)
                }
                return
            }
            guard let success = serverResponse["success"] as? Bool else{
                DispatchQueue.main.async {
                    self.present(Globals.instance.showMessage(title: "Attention", message: "An error was encountered, please try again later", actionHandler: nil), animated: true, completion: nil)
                    _=self.navigationController?.popViewController(animated: true)
                }
                return
            }
            if success{
                DispatchQueue.main.async {
                    self.present(Globals.instance.showMessage(title: "Success", message: "Your poll was created successfully!", actionHandler: nil), animated: true, completion: nil)
                    _=self.navigationController?.popViewController(animated: true)
                }
                return
            }else{
                DispatchQueue.main.async {
                    self.present(Globals.instance.showMessage(title: "Attention", message: "An error was encountered, please try again later", actionHandler: nil), animated: true, completion: nil)
                    _=self.navigationController?.popViewController(animated: true)
                }
                return
            }
        }


    }

    func addBondsPressed(){
        let sb = UIStoryboard(name: "Storyboard", bundle: nil)
        let bondChooser = sb.instantiateViewController(withIdentifier: "bondchooser") as! UINavigationController
        let temp = bondChooser.viewControllers.first as! BondChooserVC
        temp.delegate = self
        present(bondChooser, animated: true, completion: nil)
    }

    func getOptions() -> [String]?{
        var options = [String]()
        optionsView.stack.subviews.forEach { (subView) in
            if let textField = subView as? UITextField{
                if let tft = textField.text, tft.characters.count != 0{
                    options.append(tft)
                }}
        }
        if options.count != 0{
            return options
        }
        return nil
        
    }

    var selectedBonds = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "New Poll"
        view.backgroundColor = Globals.instance.lightBlue
        setup()
    }
    

    func setup(){
        view.addSubview(subjectTextField)
        view.addSubview(addBonds)
        view.addSubview(optionsView)
        view.addSubview(submitBtn)
        
        subjectTextField.anchor(view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 20, leftConstant: 30, bottomConstant: 0, rightConstant: 30, widthConstant: 0, heightConstant: 40)
        addBonds.anchor(subjectTextField.bottomAnchor, left: subjectTextField.leftAnchor, bottom: nil, right:nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 100, heightConstant: 0)
        optionsView.anchor(addBonds.bottomAnchor, left: subjectTextField.leftAnchor, bottom: view.bottomAnchor, right: subjectTextField.rightAnchor, topConstant: 20, leftConstant: 0, bottomConstant: 100, rightConstant: 0, widthConstant: 0, heightConstant: 0)

        submitBtn.anchor(optionsView.bottomAnchor, left: optionsView.rightAnchor, bottom: nil, right: nil, topConstant: -25, leftConstant: -25, bottomConstant: 0, rightConstant: 0, widthConstant: 50, heightConstant: 50)
    }
    
}


class OptionsView: UIView, UITextFieldDelegate {
    
    weak var delegeta: NewPollVC? = nil
    
    var header: UILabel = {
        let l = UILabel()
        l.text = "Enter Options"
        l.textColor = .white
        return l
    }()
    
    var numberOfOptions = 1
    
    lazy var stack: UIStackView = {
        let s = UIStackView()
        s.translatesAutoresizingMaskIntoConstraints = false
        s.spacing = 10
        s.alignment = .fill
        s.distribution = .fill
        s.axis = .vertical
        return s
    }()

    var option: UITextField = {
        let t = UITextField()
        t.textColor = .white
        t.layer.masksToBounds = true
        t.background = Globals.instance.getGraident()
        t.layer.cornerRadius = 10
        let closebtn = Fbutton(frame: CGRect.zero)
        closebtn.frame.size.width = 30
        closebtn.frame.size.height = 30
        closebtn.layer.cornerRadius = 15
        closebtn.backgroundColor = .green
        t.rightView = closebtn
        return t
    }()
    
    var footer: UIButton = {
        let b = UIButton(type: .system)
        b.setTitle("Add More", for: .normal)
        b.tintColor = .white
        b.layer.masksToBounds = true
        b.layer.cornerRadius = 10
        b.layer.borderColor = UIColor.white.cgColor
        b.layer.borderWidth = 2
        b.addTarget(self, action: #selector(footerPressed), for: .touchUpInside)
        return b
    }()
    
    func footerPressed(){
        if numberOfOptions >= 5 || !checkStackForEmptyOption(){
            return
        }
        numberOfOptions += 1
        returnOptionToAdd(withNumber: numberOfOptions)
    }
    
    func checkStackForEmptyOption() -> Bool{
        var flag = true
        stack.arrangedSubviews.forEach { (subView) in
            if let text = (subView as! UITextField).text, text.characters.count != 0 {
               
            }else{
                flag = false
                return
            }
        }
        return flag
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        addSubview(header)
        addSubview(stack)
        addSubview(footer)
        
        header.anchor(topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 5, leftConstant: 10, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        stack.anchor(header.bottomAnchor, left: header.leftAnchor, bottom: nil, right: header.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        footer.anchor(stack.bottomAnchor, left: stack.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 150, heightConstant: 0)
        
        fillStackView()
    }
    
    func fillStackView(){
        for subView in stack.arrangedSubviews {
            subView.removeFromSuperview()
        }
        for i in 1...numberOfOptions{
            returnOptionToAdd(withNumber: i)
            
        }
    }
    
    func returnOptionToAdd(withNumber number: Int){
        let o = UITextField()
        o.delegate = self
        o.setAttributedPlaceholder(string: "Option \(number)")
        o.translatesAutoresizingMaskIntoConstraints = false
        o.background = Globals.instance.getGraident()
        o.layer.masksToBounds = true
        o.layer.cornerRadius = 20
        o.frame.size.height = 45
        let closebtn = CloseButton(frame: CGRect.zero)
        closebtn.frame.size.width = 30
        closebtn.frame.size.height = 30
        closebtn.layer.masksToBounds = true
        closebtn.layer.borderColor = UIColor.white.cgColor
        closebtn.layer.borderWidth = 1
        closebtn.layer.cornerRadius = 15
        closebtn.setTitle("x", for: .normal)
        closebtn.index = number
        closebtn.addTarget(self, action: #selector(deleteOption), for: .touchUpInside)
        o.rightView = closebtn
        o.rightViewMode = .always
        stack.addArrangedSubview(o)
        o.anchor(nil, left: stack.leftAnchor, bottom: nil, right: stack.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 40)
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    func deleteOption(sender: CloseButton){
        stack.removeArrangedSubview(stack.arrangedSubviews[sender.index - 1])
        numberOfOptions -= 1
        refreshPlaceholders()
    }
    
    func refreshPlaceholders(){
        var index = 1
        stack.arrangedSubviews.forEach { (subView) in
            let textField = subView as! UITextField
            textField.setAttributedPlaceholder(string: "Option \(index)")
            (textField.rightView as! CloseButton).index = index
            index += 1
        }
    }
}

class CloseButton: Fbutton{
    var index: Int = 0
}
