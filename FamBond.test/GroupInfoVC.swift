//
//  GroupInfoVC.swift
//  FamBond.test
//
//  Created by Jatin Garg on 16/01/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import UIKit
import Alamofire
class GroupInfoMembersCell: UITableViewCell{
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var profilePicture: UIImageView!

    
}
class GroupInfoVC: UIViewController {
    
    @IBOutlet weak var spacingBtwTblAndImg: NSLayoutConstraint!
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var groupImage: UIImageView!

    var prevScrollOffset: CGFloat = 0
    var groupID: String!
    var groupInfo: [String:Any]? = nil{
        didSet{
            if groupInfo != nil{
                DispatchQueue.main.async {
                    self.setgroupImage()
                    self.tableView.reloadData()
                }
            }

        }
    }

    func setgroupImage(){
        if let url = groupInfo?["picture"] as? String{
            if let pictureURL = URL(string: url){
                groupImage.sd_setImage(with: pictureURL)
                print("picsets")
            }
        }

    }

    func addBtnPressed(){
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let inviteVC = sb.instantiateViewController(withIdentifier: "invitecontacts") as! InviteVC
        inviteVC.groupID = self.groupID
        navigationController?.pushViewController(inviteVC, animated: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Bond Info"
        let rightbtn = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addBtnPressed))
        navigationItem.rightBarButtonItem = rightbtn
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        Alamofire.request(APIEndPoints.bondInfo, method: .post, parameters: ["_id":self.groupID], encoding: JSONEncoding.default, headers: nil).responseJSON{response in
            guard let serverResponse = response.value as? [String:Any]
                else{
                    DispatchQueue.main.async {
                        self.present(Globals.instance.showMessage(title: "Error", message: "A network error was encountered", actionHandler: nil), animated: true, completion: nil)
                    }
                    return
                }
            guard let success = serverResponse["success"] as? Bool
                else{
                    DispatchQueue.main.async {
                        self.present(Globals.instance.showMessage(title: "Error", message: "An internal error was encountered", actionHandler: nil), animated: true, completion: nil)
                    }
                    return
                }
            if success{
                let data = serverResponse["data"] as! [String:Any]
                self.groupInfo = data
            }else{
                DispatchQueue.main.async {
                    self.present(Globals.instance.showMessage(title: "Error", message: "Fatal error encountered", actionHandler: nil), animated: true, completion: nil)
                }
                return
            }
        }

    }


}

extension GroupInfoVC: UITableViewDataSource, UITableViewDelegate{

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let members = groupInfo?["members"] as? [[String:Any]]{
            return members.count
        }
        return 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let contact = (groupInfo!["members"] as! [[String:Any]])[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! GroupInfoMembersCell
        cell.nameLabel.text = "\(contact["firstName"] as! String) (\(contact["phone"] as! String))"
        if let url = contact["picture"] as? String{
            if let pictureUrl = URL(string: url){
                cell.profilePicture.sd_setImage(with: pictureUrl)
            }
        }
        return cell
    }


//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        //determingin the direction
//        var scrollOffset = 0
//        scrollOffset = Int(prevScrollOffset) - Int(scrollView.contentOffset.y)
//        headerHeightConstraint.constant += CGFloat(scrollOffset)
//        if prevScrollOffset < scrollView.contentOffset.y{
//            //scrolled up
//            print("up")
//        }else{
//            print("down")
//        }
//        prevScrollOffset = scrollView.contentOffset.y
//
//    }
}
