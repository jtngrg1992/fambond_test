//
//  VideoImport.swift
//  FamBond.test
//
//  Created by Jatin Garg on 05/03/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import Foundation
import AVFoundation

class VideoImport: NSObject, ProgressReporting{
    var asset: AVAsset
    var progress: Progress

    var completionHandler: ((_ localUrl: String?, _ remoteUrl: String?, _ error: NSError?)->Void)?
    init(asset: AVAsset) {
        self.asset = asset
        progress = Progress(totalUnitCount: -1)
        progress.totalUnitCount = 10
    }

    func start(){
        //need to start exporting first
        let newExport = VideoExport(avAsset: asset)
        progress.addChild(newExport.progress, withPendingUnitCount: 2)
        newExport.completionHandler = {data,error in
            if error != nil || data == nil{
                self.callCompletionHandler(localUrl: nil, remoteUrl: nil, error: error)
                return
            }
            print("export success! \(data?.count)")
            //need to start upload now
            let newUpload = MediaUpload(data: data!, uploadURL: URL(string: videouploadUrl)!)
            self.progress.addChild(newUpload.progress, withPendingUnitCount: 8)
            print("starting upload")
            newUpload.completionHandler = {url,error in
                if error != nil || url == nil{
                    self.callCompletionHandler(localUrl: nil, remoteUrl: nil, error: error as NSError?)
                    return
                }
                //save file to media directory
                let videoName = NSUUID().uuidString + ".mp4"
                let mediaURL = getMediaUrl()!.appendingPathComponent("\(videoName)")
                try! data!.write(to: mediaURL)
                self.callCompletionHandler(localUrl: videoName, remoteUrl: url, error: nil)
            }
            newUpload.startUpload(withExtension: "mp4")

        }
        newExport.exportVideo()


    }

    func callCompletionHandler(localUrl: String?,remoteUrl: String?, error: NSError?){
        completionHandler!(localUrl,remoteUrl,error)
        completionHandler = nil
    }

    func reportError(error: NSError){
        print("Error: \(error.localizedDescription)")
    }
}
