//
//  OTPVC.swift
//  FamBond.test
//
//  Created by Jatin Garg on 05/01/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class OTPVC: UIViewController, UITextFieldDelegate{
    
    var logoView: UIImageView = {
        let i = UIImageView()
        i.translatesAutoresizingMaskIntoConstraints = false
        i.image = #imageLiteral(resourceName: "icon")
        i.contentMode = .scaleAspectFit
        return i
    }()
    
    lazy var stack: UIStackView = {
        let s = UIStackView()
        s.backgroundColor = .red
        s.translatesAutoresizingMaskIntoConstraints = false
        s.spacing = 20
        s.alignment = .center
        s.distribution = .fillEqually
        s.axis = .horizontal
        for i in 1...4 {
            let f = FTextField()
            f.keyboardType = .numberPad
            f.textAlignment = .center
            f.delegate = self
            f.index = i
            s.addArrangedSubview(f)
        }
        return s
    }()
    
    var infoLabel: UILabel = {
        let l = UILabel()
        l.text = "Fambond will send an OTP to confirm your phone number.\n Carrier SMS charges may apply"
        l.adjustsFontSizeToFitWidth = true
        l.font = UIFont.systemFont(ofSize: 12)
        l.textColor = .white
        l.numberOfLines = 0
        l.textAlignment = .center
        return l
    }()
    var number: String!
    
    lazy var submitBtn: Fbutton = {
        let b = Fbutton()
        b.translatesAutoresizingMaskIntoConstraints = false
        b.setTitle("Submit", for: .normal)
        b.addTarget(self, action: #selector(submitPressed), for: .touchUpInside)
        return b
    }()
    
    var timerView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        return v
    }()
    
    var fieldBeingEdited: Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Globals.instance.lightBlue
        view.addSubview(logoView)
        view.addSubview(infoLabel)
        view.addSubview(stack)
        view.addSubview(submitBtn)
        view.addSubview(timerView)
        logoView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        logoView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.5).isActive = true
        logoView.topAnchor.constraint(equalTo: view.topAnchor, constant: 50).isActive = true
        logoView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        
        infoLabel.anchor(logoView.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 50, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: 0)
        
        stack.anchorCenterXToSuperview()
        stack.anchor(infoLabel.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 50, leftConstant: 30, bottomConstant: 0, rightConstant: 30, widthConstant: 0, heightConstant: 0)
        
        submitBtn.anchor(nil, left: view.leftAnchor, bottom: timerView.topAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 40, bottomConstant: 30, rightConstant: 40, widthConstant: 0, heightConstant: 60)
        
        timerView.anchor(nil, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 40, bottomConstant: 50, rightConstant: 40, widthConstant: 0, heightConstant: 30)
        
        setupTimerViewComponents()
    }
    
    func setupTimerViewComponents(){
        let b = UIButton(type: .system)
        b.setTitle("Resend", for: .normal)
        b.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        b.tintColor = .white
        
        timerView.addSubview(b)
        b.anchorCenterYToSuperview()
        b.anchor(nil, left: timerView.leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 20, bottomConstant: 0, rightConstant: 0, widthConstant: 50, heightConstant: 0)
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        stack.subviews.forEach { (sub) in
            sub.resignFirstResponder()
        }
    }
    func submitPressed(){
        let indicator = self.showActivityIndicator()
        //checking if registered
        Alamofire.request(APIEndPoints.checkRegisteration, method: .post, parameters: ["phone":self.number], encoding: JSONEncoding.default, headers: nil).responseJSON{response in
            indicator.animate = false
            let serverResponse = response.value as? [String : Any]
            print(serverResponse as Any)
            guard let success = serverResponse?["success"] as? Bool else{
                DispatchQueue.main.async {
                    self.present(Globals.instance.showMessage(title: "Attention", message: "Some error was encountered, please try again later", actionHandler: nil), animated: true, completion: nil)
                }
                return
            }
            if success{
                let isRegistered = serverResponse?["isRegistered"] as! Bool
                if isRegistered{
                    let data = serverResponse?["data"] as! [String : Any]
                    let user = UserInfo(_id: data["_id"] as! String, firstName: data["firstName"] as! String,lastName: data["lastName"] as! String, phone: data["phone"] as! String, picture: data["picture"] as? String, dob: data["dob"] as! String, gender: data["gender"] as! String)
                    UserDefaults.standard.set(user.toDict(), forKey: "userInfo")

                    //log the user in
                    SocketIOManager.sharedInstace.connectWithPhone(phone: user.phone, completionHandler: { (info) in
                        //might put up a notification observer
                    })
                    let tab = CustomTabBarController()
                    DispatchQueue.main.async {
                        self.present(tab, animated: true, completion: nil)
                    }

                }else{
                    let sb = UIStoryboard(name: "Registration", bundle: nil)
                    let personal = sb.instantiateViewController(withIdentifier: "personal")as! PersonalDetailVC
                    personal.number = self.number
                    DispatchQueue.main.async {
                        self.present(personal, animated: true, completion: nil)
                    }
                }
            }else{
                //not success
                DispatchQueue.main.async {
                    self.present(Globals.instance.showMessage(title: "Attention", message: "Some error was encountered, please try again later", actionHandler: nil), animated: true, completion: nil)
                }
            }
        }

    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentCharacterCount = textField.text?.characters.count ?? 0
        if (range.length + range.location > currentCharacterCount){
            return false
        }
        let newLength = currentCharacterCount + string.characters.count - range.length
        let flag = newLength <= 1
        if (!flag){
            //time to switch text fields
            let f = textField as! FTextField
            switchTextField(from: f)
        }
        return newLength <= 1
    }
    
    
    func switchTextField(from: FTextField){
        guard let fromIndex = from.index
            else{
                return
        }
        let targetIndex = fromIndex + 1
        stack.subviews.forEach { (v) in
            if let f = v as? FTextField{
                if f.index! != targetIndex{
                    f.resignFirstResponder()
                }else{
                    f.becomeFirstResponder()
                }
            }
        }
    }
    
}
