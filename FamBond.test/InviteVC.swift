//
//  InviteVC.swift
//  FamBond.test
//
//  Created by Jatin Garg on 25/02/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import UIKit
import CoreData
import Alamofire

class InviteVCCell: UITableViewCell{

    @IBOutlet weak var nameLabel: PriorityLabel!
    @IBOutlet weak var phoneLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        nameLabel.isSelected = false
        nameLabel.layer.cornerRadius = 0
        nameLabel.selectIndicator.layer.cornerRadius = 10
    }

}

class InviteVC: UITableViewController {

    var selectedContact: String? = nil
    var groupID: String!
    var contacts: [Contact]?{
        didSet{
            tableView.reloadData()

        }
    }
    let pvtContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)

    override func viewDidLoad() {
        super.viewDidLoad()
        pvtContext.parent = DBManager.getContext()
        pvtContext.perform {
            self.contacts = Contact.getContacts(context: self.pvtContext)
        }
        let done = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(donePressed))
        navigationItem.rightBarButtonItem = done
    }

    func donePressed(){
        //checks
        if selectedContact == nil{
            present(Globals.instance.showMessage(title: "Attention", message: "Atleast one contact needs to be selected for this action", actionHandler: nil), animated: true, completion: nil)
            return
        }
        let  activity = showActivityIndicator()
        let parameter = ["_id":selectedContact!,"group_id":groupID!]
        print(parameter)
        Alamofire.request(APIEndPoints.addBondMember, method: .post, parameters: parameter, encoding: JSONEncoding.default, headers: nil).responseJSON{
            response in
            DispatchQueue.main.async {
                activity.animate = false
                _=self.navigationController?.popViewController(animated: true)
            }
            guard let serverResponse = response.value as? [String:Any]
                else{
                    DispatchQueue.main.async {
                        self.present(Globals.instance.showMessage(title: "Error", message: "A network error was encountered", actionHandler: nil), animated: true, completion: nil)
                    }
                    return
            }
            guard let success = serverResponse["success"] as? Bool
                else{
                    DispatchQueue.main.async {
                        self.present(Globals.instance.showMessage(title: "Error", message: "An internal error was encountered", actionHandler: nil), animated: true, completion: nil)
                    }
                    return
            }
            if success{
                DispatchQueue.main.async {
                    self.present(Globals.instance.showMessage(title: "Success", message: "Member has been added to group", actionHandler: nil), animated: true, completion: nil)
                }
                return

            }else{
                let error = serverResponse["errors"] as! String
                print(error)
                DispatchQueue.main.async {
                    self.present(Globals.instance.showMessage(title: "Error", message: "Fatal error encountered", actionHandler: nil), animated: true, completion: nil)
                }
                return
            }

        }
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if contacts == nil{
            return 0
        }
        return contacts!.count
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var contact = Contact()
        pvtContext.performAndWait {
            contact = self.contacts![indexPath.row]
        }

        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! InviteVCCell
        cell.nameLabel.text = contact.name
        cell.phoneLabel.text = contact.phone
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! InviteVCCell
        //removing highlight from all the other cells
        for subView in tableView.subviews{
            for sub in subView.subviews{
                if let cell = sub as? InviteVCCell{
                    cell.nameLabel.isSelected = false
                }
            }
        }
        cell.nameLabel.isSelected = true
        var _id = ""
        pvtContext.performAndWait {
            _id = self.contacts![indexPath.row].id!
        }
        self.selectedContact = _id
    }

}
