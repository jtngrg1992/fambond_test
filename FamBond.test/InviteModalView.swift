//
//  InviteModalView.swift
//  JamesBond
//
//  Created by Jatin Garg on 01/11/16.
//  Copyright © 2016 Jatin Garg. All rights reserved.
//

import UIKit
import MessageUI

protocol InviteModalViewDelegate: class{
    func modalClosed()
    func whatsAppPressed()
    func messagePressed(numbers: [String])
}

class InviteModalView: UIView, MFMessageComposeViewControllerDelegate {

    var recipients = [String]()
    weak var delegate: InviteModalViewDelegate? = nil
    @IBOutlet weak var closeBtn: UIButton!
    @IBAction func whatsappIconPressed(_ sender: Any) {
        delegate?.whatsAppPressed()
    }
    @IBAction func messagedIconPressed(_ sender: Any) {
        delegate?.messagePressed(numbers: recipients)
        
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
       controller.dismiss(animated: true, completion: nil)
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    @IBAction func closeBtnPressed(_ sender: Any) {
        delegate?.modalClosed()
    }
    
    func setUp()
    {
        let nib = Bundle.main.loadNibNamed("InviteModalView", owner: self, options: nil)?.first as! UIView
//        nib.bounds = self.bounds
        nib.backgroundColor = Globals.instance.secondColor
        nib.frame.size.width = self.frame.width
        nib.frame.size.height = self.frame.height
        self.addSubview(nib)
        
    }

}
