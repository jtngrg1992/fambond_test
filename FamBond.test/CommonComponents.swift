//
//  CommonComponents.swift
//  FamBond.test
//
//  Created by Jatin Garg on 22/01/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class SelectableBtn: UIButton{
    @IBInspectable var wasSelected: Bool = false {
        didSet{
            if wasSelected{
                backgroundColor = Globals.instance.secondColor
            }else{
                backgroundColor = Globals.instance.darkBlue
            }
            setNeedsDisplay()
        }
    }
}
@IBDesignable class RoundBtn: UIButton{
    @IBInspectable var cornerRadius: CGFloat = 0{
        didSet{
            updateUI()
        }
    }
    @IBInspectable var borderWidth: CGFloat = 0{
        didSet{
            updateUI()
        }
    }
    @IBInspectable var borderColor: UIColor = UIColor.white{
        didSet{
            updateUI()
        }
    }

    func updateUI(){
        layer.masksToBounds = true
        layer.cornerRadius = cornerRadius
        layer.borderColor = borderColor.cgColor
        layer.borderWidth = borderWidth
        setNeedsDisplay()
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        backgroundColor = Globals.instance.secondColor
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        backgroundColor = .clear
    }
}
class CustomTabBarController: UITabBarController{
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let contactsVC = sb.instantiateViewController(withIdentifier: "contacts")
        let feedsVC = sb.instantiateViewController(withIdentifier: "feed")
        let first = UINavigationController(rootViewController: AppHomeVC())
        first.tabBarItem.image = UIImage(named: "bond-tab-unhighlighted")?.withRenderingMode(.alwaysOriginal)
        first.tabBarItem.selectedImage = UIImage(named: "bond-tab-highlighted")?.withRenderingMode(.alwaysOriginal)
        
        let second = contactsVC
        second.tabBarItem.image = UIImage(named: "contacts-tab-unhighlighted")?.withRenderingMode(.alwaysOriginal)
        second.tabBarItem.selectedImage = UIImage(named: "contacts-tab-highlighted")?.withRenderingMode(.alwaysOriginal)

        feedsVC.tabBarItem.image = UIImage(named: "feed-tab-unhighlighted")?.withRenderingMode(.alwaysOriginal)
        feedsVC.tabBarItem.selectedImage = UIImage(named: "feed-tab-highlighted")?.withRenderingMode(.alwaysOriginal)


        viewControllers = [first,feedsVC,second]
    }
    func setup(){
        tabBar.isTranslucent = false
        tabBar.barTintColor = Globals.instance.darkBlue
    }
}


class UpdatingNavigationHeader: UIView {
    var activityIndicator: UIActivityIndicatorView = {
        let a = UIActivityIndicatorView()
        a.startAnimating()
        a.hidesWhenStopped = true
        a.color = .white
        return a
    }()
    
    var descriptionLabel: UILabel = {
        let l = UILabel()
        l.font = UIFont.boldSystemFont(ofSize: 17)
        l.text = "Updating.."
        l.textColor = .white
        return l
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        addSubview(activityIndicator)
        addSubview(descriptionLabel)
        
        addConstraintsWithFormat("H:|[v0]-10-[v1]|", views: activityIndicator,descriptionLabel)
        activityIndicator.anchorCenterYToSuperview()
        descriptionLabel.anchorCenterYToSuperview()
        
    }
}

class ExportingNavigationHeader: UIView {
    var activityIndicator: UIActivityIndicatorView = {
        let a = UIActivityIndicatorView()
        a.startAnimating()
        a.hidesWhenStopped = true
        a.color = .black
        return a
    }()
    
    var descriptionLabel: UILabel = {
        let l = UILabel()
        l.font = UIFont.boldSystemFont(ofSize: 17)
        l.text = "Updating.."
        return l
    }()
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        addSubview(activityIndicator)
        addSubview(descriptionLabel)
        
        addConstraintsWithFormat("H:|[v0]-10-[v1]|", views: activityIndicator,descriptionLabel)
        activityIndicator.anchorCenterYToSuperview()
        descriptionLabel.anchorCenterYToSuperview()
        
    }
}
