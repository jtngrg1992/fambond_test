//
//  PersonalDetailVC.swift
//  FamBond.test
//
//  Created by Jatin Garg on 01/03/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import UIKit
import Alamofire

class PersonalDetailVC: UIViewController{

    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var dob: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var genderStack: UIStackView!
    @IBOutlet weak var male: SelectableBtn!
    @IBOutlet weak var female: SelectableBtn!
    @IBOutlet weak var other: SelectableBtn!
    var selectedGender: String = "Male"
    var url: String? = nil
    var number: String!
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.subviews.forEach { (v) in
            if v is UITextField{
                v.resignFirstResponder()
            }
        }
    }
    @IBAction func editProfileImage(_ sender: Any) {
        let picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.sourceType = .photoLibrary
        picker.delegate = self
        present(picker, animated: true, completion: nil)
    }

    @IBAction func maleTapped(_ sender: Any) {
       toggleGender(sender: sender)
    }

    @IBAction func femaleTapped(_ sender: Any) {
        toggleGender(sender: sender)
    }

    @IBAction func otherTapped(_ sender: Any) {
        toggleGender(sender: sender)
    }

    @IBAction func submitPressed(_ sender: Any) {
        for tf in [firstName,lastName,dob]{
            if tf?.text?.trimmingCharacters(in: .whitespacesAndNewlines).characters.count == 0{
                tf?.shake()
                return
            }
        }
        let indicator = self.showActivityIndicator()
        //perform registeration
        let parameters = ["firstName":firstName.text!.trimmingCharacters(in: .whitespacesAndNewlines),"lastName": lastName.text!.trimmingCharacters(in: .whitespacesAndNewlines),"dob":dob.text!,"gender":selectedGender,"phone":self.number,
                          "picture":self.url ?? "","platform":"ios"] as [String : Any]
        Alamofire.request(APIEndPoints.register, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON{response in
            DispatchQueue.main.async {
                indicator.animate = false
            }
            let responseValue = response.value as? [String : Any]
            guard let success = responseValue?["success"] as? Bool else{return}
            if success{
                //save data in local storage
                guard let data = responseValue?["data"] as? [String : Any] else{return}
                let userInfo = UserInfo(_id: data["_id"] as! String, firstName: data["firstName"] as! String, lastName: data["lastName"] as! String, phone: data["phone"] as! String, picture: self.url, dob: data["dob"] as! String, gender: data["gender"] as! String)
                UserDefaults.standard.setValue(userInfo.toDict(), forKey: "userInfo")
                //login
                SocketIOManager.sharedInstace.connectWithPhone(phone: userInfo.phone, completionHandler: { (info) in
                    //might put up a notification handler
                })
                let tab = CustomTabBarController()
                DispatchQueue.main.async {
                    self.present(tab, animated: true, completion: nil)
                }
            }else{
                    DispatchQueue.main.async {
                        self.present(Globals.instance.showMessage(title: "Attention", message: "An internal error was encountered", actionHandler: nil), animated: true, completion: nil)
                    }
            }

        }

    }

    func toggleGender(sender: Any){
        for subview in genderStack.subviews{
            let btn = subview as! SelectableBtn
            btn.wasSelected = false
        }
        (sender as! SelectableBtn).wasSelected = true
        selectedGender = ((sender as! SelectableBtn).titleLabel?.text)!
    }

    func uploadImageToServer(image: UIImage){
        if let formData = UIImageJPEGRepresentation(image, 0.7){
            let mimeType = "image/jpeg"
            let uploadUrl = URL(string : fileuploadUrl)!
            Alamofire.upload(multipartFormData: { (mpFormdata) in
                mpFormdata.append(formData, withName: "file",fileName: "yo.jpg", mimeType: mimeType)
            }, to: uploadUrl, encodingCompletion: { (result) in
                switch(result){
                case .success(let upload, _ ,_):
                    upload.uploadProgress(closure: {(progress) in
                        //handler this progress
                        print(progress.fractionCompleted)
                    })
                    upload.responseJSON(completionHandler: { (response) in
                        switch response.result{
                        case .success(let value):
                            let responseValue = value as? [String : Any]
                            if let url = responseValue?["Url"] as? String{
                                self.profileImage.image = image
                                self.url = url
                            }else{
                                DispatchQueue.main.async {
                                    self.present(Globals.instance.showMessage(title: "Attention", message: "There was an error uploading your picture, try again later!", actionHandler: nil), animated: true, completion: nil)
                                }
                            }
                            break
                        default:
                            DispatchQueue.main.async {
                                self.present(Globals.instance.showMessage(title: "Attention", message: "There was an error uploading your picture, try again later!", actionHandler: nil), animated: true, completion: nil)
                            }
                            break
                        }
                    })
                    break
                case .failure(let error):
                    let alert = Globals.instance.showMessage(title: "Attention", message: "Error uploading image file: \(error)", actionHandler: nil)
                    self.present(alert, animated: true, completion: nil)

                }
            })
        }

    }

    func datePickerChanged(sender: UIDatePicker){
        let formater = DateFormatter()
        formater.dateFormat = "MM-dd-yyyy"
        let selectedDate = sender.date
        let stringDate = formater.string(from: selectedDate)
        dob.text = stringDate
    }
}

extension PersonalDetailVC: UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerEditedImage] as! UIImage
        uploadImageToServer(image: chosenImage)
        dismiss(animated: true, completion: nil)

    }
}

extension PersonalDetailVC: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == dob{
            let datePicker = UIDatePicker()
            datePicker.maximumDate = Date()
            datePicker.datePickerMode = .date
            datePicker.addTarget(self, action: #selector(datePickerChanged), for: .valueChanged)
            textField.inputView = datePicker
        }

        
    }
}

