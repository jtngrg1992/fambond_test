//
//  DBManager.swift
//  JamesBond
//
//  Created by Jatin Garg on 28/11/16.
//  Copyright © 2016 Jatin Garg. All rights reserved.
//

import Foundation
import CoreData

class DBManager: NSObject{
    // MARK: - Core Data stack
    
    static var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "FamBond_test")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    static var privateQueueContext: NSManagedObjectContext = {
        let p = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        p.parent = getContext()
        return p
    }()
    
    class func getContext() -> NSManagedObjectContext{
        return persistentContainer.viewContext
    }
    // MARK: - Core Data Saving support
    
    
    class func performBackgroundTask(_ block: @escaping (NSManagedObjectContext) -> Void) {
        self.persistentContainer.performBackgroundTask(block)
    }
    class  func performForegroundTask(_ block: @escaping (NSManagedObjectContext) -> Void) {
        self.getContext().perform {
            block(self.getContext())
        }
    }
    
    class func saveContext () {
        let context = getContext()
        if context.hasChanges {
            do {
                try context.save()
                print("Saved Context")
            } catch {
                let nserror = error as NSError
                fatalError("CoreData save context error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}
