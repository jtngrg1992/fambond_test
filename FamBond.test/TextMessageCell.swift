//
//  TextMessageCell.swift
//  FamBond.test
//
//  Created by Jatin Garg on 02/03/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import UIKit

fileprivate var textMessageKVOContext = 0
class TextMessageCell: UICollectionViewCell {

    @IBOutlet var nameLabelHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var nameLabel: UILabel!
    private let deliveryKeyPath = "isDelivered"
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var deliveryStatusImg: UIImageView!
    @IBOutlet weak var bubbleView: UIView!
    @IBOutlet weak var textView: UILabel!
    @IBOutlet var bubbleLeadingConstraint: NSLayoutConstraint!
    @IBOutlet var bubbleTrailingConstraint: NSLayoutConstraint!
    @IBOutlet var bubbleWidthConstraint: NSLayoutConstraint!
    private var senderName: String?{
        didSet{
            if let newName = senderName, !message!.isSender{
                nameLabel.text = newName
                nameLabelHeightConstraint.constant = 20
            }else{
                nameLabel.text = ""
                nameLabelHeightConstraint.constant = 0
            }
        }
    }

    weak var parent: ChatlogV2VC? 

    var message: Message_CD?{
        willSet{
            if let formerMessage = message{
                formerMessage.removeObserver(self, forKeyPath: deliveryKeyPath, context: &textMessageKVOContext)
            }
        }
        didSet{
            if let newMessage = message{
                newMessage.addObserver(self, forKeyPath: deliveryKeyPath, options: [], context: &textMessageKVOContext)
                senderName = getSenderName()
            }
            updateDelivery()
            updateText()
            updateBubbleAlignment()
            updateTime()
        }
    }

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        guard context == &textMessageKVOContext else{
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
            return
        }
        OperationQueue.main.addOperation {
            if keyPath == self.deliveryKeyPath{
                self.updateDelivery()
            }
        }

    }

    private func getSenderName() -> String?{
        guard let senderJSON = message!.sender else {return nil}
        guard let senderData = senderJSON.data(using: .utf8) else {return nil}
        guard let parsedSenderData = (try? JSONSerialization.jsonObject(with: senderData, options: [])) else{return nil}
        guard let senderDictionary = parsedSenderData as? [String:Any] else {return nil}
        guard let senderName = senderDictionary["name"] as? String else {return nil}
        return senderName

    }
    private func updateText(){
        if let text = message?.text?.trimmingCharacters(in: .whitespacesAndNewlines){
            textView.text = text
        }else{
            textView.text = ""
        }
    }

    private func updateTime(){
        guard let time = message!.time else {return}
        let formattedTme = formatMesageTime(date: time as Date)
        timeLabel.text = formattedTme
    }

    private func updateBubbleAlignment(){
        bubbleTrailingConstraint.isActive = message!.isSender
        bubbleLeadingConstraint.isActive = !message!.isSender
        bubbleView.backgroundColor = message!.isSender ? Globals.instance.sentChatColor : Globals.instance.receivedChatColor
        textView.textColor = message!.isSender ? UIColor.black : UIColor.white
        let estimatedFrame = getEstimatedFrame(forText: message!.text!)
        bubbleWidthConstraint.constant = estimatedFrame.width + 25
      }

    private func updateDelivery(){
        if !message!.isSender{
            deliveryStatusImg.isHidden = true
            return
        }
        deliveryStatusImg.isHidden = false
        if message!.isDelivered {
            deliveryStatusImg.image = #imageLiteral(resourceName: "double-tick")
        }else{
            deliveryStatusImg.image = #imageLiteral(resourceName: "wait")
        }
    }

    private func getEstimatedFrame(forText text: String) -> CGRect{
        //finding which string is bigger and calculating width accordingly
        var senderString = ""
        if senderName != nil{
            senderString = senderName!
        }
        let stringForWidth = (text.characters.count >= senderString.characters.count) ?  text : senderString
        let size = CGSize(width: maxBubbleWidth, height: 1000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        let estimatedFrame = NSString(string: stringForWidth).boundingRect(with: size, options: options, attributes:[NSFontAttributeName: UIFont.systemFont(ofSize: messageTextSize) ], context: nil)
        return estimatedFrame
    }

    func removeObservers(){
        message?.removeObserver(self, forKeyPath: deliveryKeyPath, context: &textMessageKVOContext)
    }
    
    deinit {
        removeObservers()
    }

}

//MARK:- Supplementary actions

extension TextMessageCell{

    override func awakeFromNib() {
        super.awakeFromNib()
        //shadow stuf
        bubbleView.layer.shadowColor = UIColor.black.cgColor
        bubbleView.layer.shadowRadius = 1
        bubbleView.layer.shadowOffset = CGSize(width: 0, height: 0)
        bubbleView.layer.shadowOpacity = 0.8
        bubbleView.layer.shouldRasterize = true
        bubbleView.layer.rasterizationScale = UIScreen.main.scale

        //pan stuff
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(viewPanned))
        bubbleView.addGestureRecognizer(panGesture)
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(longPressed))
        longPressGesture.minimumPressDuration = 0.4
        bubbleView.addGestureRecognizer(longPressGesture)
    }

    func viewPanned(gesture: UIPanGestureRecognizer){
            //isolating conditions necessary for ignoring the gesture
        if !message!.isSender{return}
        let translation = gesture.translation(in: self.contentView)
        if translation.x > 0 {return}

            //updating contstraint constant to change the position
        bubbleTrailingConstraint.constant -= translation.x
        gesture.setTranslation(.zero, in: self.contentView)

            //detecting when the new controller should be pushed
        if bubbleTrailingConstraint.constant > 50{
            let sb = UIStoryboard(name: "Chatlog", bundle: nil)
            let messageInfo = sb.instantiateViewController(withIdentifier: "messageinfo") as! MessageInfoVC
            messageInfo.msgToDisplay = self.message
            parent?.navigationController?.pushViewController(messageInfo, animated: true)
            UIView.animate(withDuration: 0.3, animations: {
                self.bubbleTrailingConstraint.constant = 10
                self.layoutIfNeeded()
            })
            return
        }

    }

    func longPressed(sender: UILongPressGestureRecognizer){
        if sender.state == .began{
            becomeFirstResponder()
            let menuController = UIMenuController.shared
            let forward = UIMenuItem(title: "forward", action: #selector(forwardPressed))
            let copy = UIMenuItem(title: "copy", action: #selector(copyPressed))
            menuController.menuItems = [forward,copy]
            menuController.update()
            menuController.setTargetRect(bubbleView.bounds, in: bubbleView)
            menuController.setMenuVisible(true , animated: true)
        }

    }

    func forwardPressed(){
        parent?.forwardMessage(message: message!)
    }

    func copyPressed(){

    }

    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        super.canPerformAction(action, withSender: sender)
        if action == #selector(copyPressed){
            return true
        }
        if action == #selector(forwardPressed){
            return true
        }
        return false

    }

    override var canBecomeFirstResponder: Bool{
        return true
    }

}
