//
//  TaskCreate.swift
//  FamBond.test
//
//  Created by Jatin Garg on 15/02/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//
import Alamofire
import Foundation
import UIKit
import BEMCheckBox

let rightViewBtnSize: CGFloat = 40
let priorityCheckSize: CGFloat = 20

class TaskCreationVC: UIViewController, UITextFieldDelegate{

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var taskName: UITextField!
    @IBOutlet weak var assignBtn: RoundBtn!
    @IBOutlet weak var taskNotes: UITextField!
    @IBOutlet weak var taskDate: UITextField!
    @IBOutlet weak var tasktime: UITextField!


    @IBAction func submitPressed(_ sender: Any) {
        //check for validity
        if taskName.text!.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty{
            present(Globals.instance.showMessage(title: "Attention", message: "Task name is a required field", actionHandler: nil), animated: true, completion: nil)
            return
        }
        if taskDate.text!.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty{
            present(Globals.instance.showMessage(title: "Attention", message: "Task date is a required field", actionHandler: nil), animated: true, completion: nil)
            return
        }
        if tasktime.text!.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty{
            present(Globals.instance.showMessage(title: "Attention", message: "Task time is a required field", actionHandler: nil), animated: true, completion: nil)
            return
        }
        let userInfo = UserDefaults.standard.value(forKey: "userInfo") as! [String:Any]
        let _id = userInfo["_id"] as! String
        selectedContacts.append(_id)
        let parameters = ["subject":taskName.text!.trimmingCharacters(in: .whitespacesAndNewlines),"note":taskNotes.text!.trimmingCharacters(in: .whitespacesAndNewlines),"scheduled_date":taskDate.text!,"scheduled_time":tasktime.text!,"users":selectedContacts,"created_by":_id] as [String : Any]
        let activity = showActivityIndicator()
        Alamofire.request(APIEndPoints.createStickie, method: .post, parameters: parameters, encoding: JSONEncoding
            .default, headers: nil).responseJSON{response in
                DispatchQueue.main.async {
                    activity.animate = false
                    _=self.navigationController?.popViewController(animated: true)
                }
            guard let serverResponse = response.value as? [String:Any] else{
                DispatchQueue.main.async {
                    self.present(Globals.instance.showMessage(title: "Attention",message: "Some error was encountered, try again later", actionHandler: nil), animated: true, completion: nil)
                }
                return
            }
            guard let success = serverResponse["success"] as? Bool else{
                DispatchQueue.main.async {
                    self.present(Globals.instance.showMessage(title: "Attention", message: "Some error was encountered, try again later", actionHandler: nil), animated: true, completion: nil)
                }
                return
            }
            if success{
                DispatchQueue.main.async {
                    self.present(Globals.instance.showMessage(title: "Success", message: "Your task was created successfully!", actionHandler: nil), animated: true, completion: nil)
                }
            }else{
                let error = serverResponse["errors"] as! String
                DispatchQueue.main.async {
                    self.present(Globals.instance.showMessage(title: "Attention",message: "\(error)", actionHandler: nil), animated: true, completion: nil)
                }
            }
        }
    }

    @IBAction func assignBtnPressed(_ sender: Any) {
    }

    var selectedContacts = [String]()

    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == taskDate{
            let datePicker = UIDatePicker()
            datePicker.datePickerMode = .date
            datePicker.minimumDate = Date()
            let calender = Calendar(identifier: .gregorian)
            var components = DateComponents()
            components.day = 60
            datePicker.maximumDate = calender.date(byAdding: components, to: Date())
            textField.inputView = datePicker
            datePicker.addTarget(self, action: #selector(dateChanged), for: .valueChanged)
            return
        }
        if textField == tasktime{
            let timePicker = UIDatePicker()
            timePicker.datePickerMode = .time
            timePicker.addTarget(self, action: #selector(timeChanged), for: .valueChanged)
            textField.inputView = timePicker

        }
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField==tasktime || textField==taskDate{
            return false
        }
        return true
    }

    func timeChanged(sender: UIDatePicker){
        let formater = DateFormatter()
        formater.dateFormat = "hh:mm a"
        let selectedDate = sender.date
        let stringDate = formater.string(from: selectedDate)
        tasktime.text = stringDate
    }

    func dateChanged(sender: UIDatePicker){
        let formater = DateFormatter()
        formater.dateFormat = "MM-dd-yyyy"
        let selectedDate = sender.date
        let stringDate = formater.string(from: selectedDate)
        taskDate.text = stringDate
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? ContactsChooserVC{
            destinationVC.delegate = self
            print("set delegate")
        }
    }
    

}



class PriorityLabel: UILabel {
    var isSelected: Bool = false{
        didSet{
            selectIndicator.backgroundColor = (isSelected) ? Globals.instance.secondColor : UIColor.black
        }
    }
    var selectIndicator: UIView = {
        let v = UIView()
        v.layer.masksToBounds = true
        v.layer.cornerRadius = priorityCheckSize/2
        return v
    }()

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    func setup(){
        isUserInteractionEnabled = true
        font = UIFont.systemFont(ofSize: 12)
        layer.masksToBounds = true
        textColor = .white
        layer.cornerRadius = 10
        insertSubview(selectIndicator, at: 0)
        selectIndicator.anchor(nil, left: nil, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 10, widthConstant: priorityCheckSize, heightConstant: priorityCheckSize)
        selectIndicator.anchorCenterYToSuperview()
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        transform = CGAffineTransform.init(scaleX: 1.1, y: 1.1)
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 6, options: .allowUserInteraction, animations: {
            self.transform = CGAffineTransform.identity
        }, completion: nil)
    }
}

