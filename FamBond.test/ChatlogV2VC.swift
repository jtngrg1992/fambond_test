//
//  ChatlogV2VC.swift
//  FamBond.test
//
//  Created by Jatin Garg on 02/03/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import UIKit
import CoreData
import QBImagePickerController
import AVFoundation
import AVKit
import Alamofire

private var textMessageIdentifier = "text"
private var imageMessageIdentifier = "image"
private var videoMessageIdentifier = "video"
fileprivate var imageExportSize = CGSize(width: 1024, height: 768)
let maxBubbleWidth = UIScreen.main.bounds.width - 60
var messageTextSize: CGFloat = 14
var maxBubbleHeight: CGFloat = 300

let standardFontSize: CGFloat = 14
let senderFontSize: CGFloat = 12
let bytesInMB = 1048576
let fileuploadUrl = "http://jlabs.co/socket/imageupload/upload.php"
let videouploadUrl = "http://jlabs.co/socket/imageupload/video.php"

public func formatMesageTime(date: Date) ->String{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "h:mm a"
    let time = dateFormatter.string(from: date)
    return time
}

class ChatlogV2VC: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var moreInputsBtn: UIButton!
    @IBOutlet weak var moreInputsView: UIView!
    @IBOutlet weak var moreInputViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var messageTextView: UITextView!

    var scrollFlag = false
    var privateContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
    var moreInputViewToggled: Bool = false
    var fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>!

    var group: Group_CD!
    var initialLoad: Bool? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        privateContext.parent = DBManager.getContext()
        setupFRC()
        initialLoad = true
     }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardShow), name: .UIKeyboardWillHide, object: nil)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if initialLoad != nil{
            initialLoad = nil
            guard let count = fetchedResultsController.sections?[0].numberOfObjects else{return}
            if count != 0{
                let indexPath = IndexPath(row: count - 1, section: 0)
                collectionView.scrollToItem(at: indexPath, at: .bottom, animated: true)
            }
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
        initialLoad = nil
    }

    @IBAction func moreInputsBtnPressed(_ sender: Any) {
        moreInputViewWidthConstraint.constant = !moreInputViewToggled ?
            140 : 40
        moreInputViewToggled = !moreInputViewToggled
        let alphaValue = moreInputViewToggled ? 1 : 0
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.3, initialSpringVelocity: 0, options: .curveEaseIn, animations: { 
            self.moreInputsView.subviews.forEach{
                $0.alpha = CGFloat(alphaValue)
            }
            self.view.layoutIfNeeded()
        }, completion: nil)
        moreInputsBtn.alpha = 1
    }


    @IBAction func galleryBtnPressed(_ sender: Any) {
        let picker = QBImagePickerController()
        picker.delegate = self
        picker.allowsMultipleSelection = true
        picker.maximumNumberOfSelection = 10
        picker.showsNumberOfSelectedAssets = true
        picker.modalPresentationStyle = .overCurrentContext
        present(picker, animated: true, completion: nil)
    }

    func setupFRC(){
        let fetchReq = NSFetchRequest<NSFetchRequestResult>(entityName: "Message_CD")
        fetchReq.predicate = NSPredicate(format: "group.mongoid==%@", (group?.mongoid)!)
        fetchReq.sortDescriptors = [NSSortDescriptor(key: "time", ascending: true)]
        fetchReq.fetchBatchSize = 10
        fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchReq, managedObjectContext: DBManager.getContext(), sectionNameKeyPath: nil, cacheName: nil)
        fetchedResultsController.delegate = self
        (try? fetchedResultsController.performFetch())
    }

    func handleKeyboardShow(notification: Notification){
        if let userinfo = notification.userInfo{
            let keyboardFrame = (userinfo[UIKeyboardFrameEndUserInfoKey] as AnyObject).cgRectValue
            let isKeyBoardShowing = notification.name == .UIKeyboardWillShow
            self.bottomConstraint.constant = isKeyBoardShowing ? (keyboardFrame?.height)! : 0
            UIView.animate(withDuration: 0, delay: 0, options: .curveEaseOut, animations: {
                self.view.layoutIfNeeded()
            }, completion: {(complete) in
                //reserve for scrolling to last indexpath
            })
        }
    }


    @IBAction func sendButtonPressed(_ sender: Any) {
        let text = messageTextView.text.trimmingCharacters(in: .whitespacesAndNewlines)
        if text.characters.count > 0{
            let messageSender = MessageSender(type: "text", text: text, localMediaURL: nil, remoteMediaURL: nil, thumbURL: nil, bondID: group.mongoid, bondName: group.name, thumb: nil)
            messageSender.shouldAddToDB = true
            messageSender.shouldSendToServer = true
            messageSender.send()
        }
    }
}


//MARK: - QBImagePicker Delegates & subroutines

extension ChatlogV2VC: QBImagePickerControllerDelegate{
    func qb_imagePickerControllerDidCancel(_ imagePickerController: QBImagePickerController!) {
        imagePickerController.dismiss(animated: true, completion: nil)
    }

    func qb_imagePickerController(_ imagePickerController: QBImagePickerController!, didFinishPickingAssets assets: [Any]!) {
        imagePickerController.dismiss(animated: true, completion: nil)
        processAssetsFromQB(assets: assets as! [PHAsset])
    }

    func processAssetsFromQB(assets: [PHAsset]){
        for asset in assets{
            let assetType = asset.mediaType.rawValue
            switch assetType{
            case 1: //image
                processAndSendImage(inAsset: asset)
            case 2: //video
                processAndSendVideo(inAsset: asset)
                break
            default: break
            }
        }
    }

    func processAndSendVideo(inAsset asset: PHAsset){
        let manager = PHImageManager.default()
        let requestOptions = PHVideoRequestOptions()
        requestOptions.version = .original

        manager.requestAVAsset(forVideo: asset, options: requestOptions) { (avAsset, audioAsset, info) in
            //inside background queue
            guard let asset = avAsset else {return}
            let  (im,ur) = self.getThumb(fromAsset: asset)
            if let thumbImage = im, let thumbURL = ur{
                guard let urlAsset = asset as? AVURLAsset else{return}
                //getting bond id name bond name
                var bondID = ""
                var bondName = ""
                self.fetchedResultsController.managedObjectContext.performAndWait {
                    bondID = self.group.mongoid!
                    bondName = self.group.name!
                }

                let sender = MessageSender(type: "video", text: nil, localMediaURL: urlAsset.url.absoluteString, remoteMediaURL: nil, thumbURL: thumbURL, bondID: bondID, bondName: bondName, thumb: thumbImage)
                sender.shouldAddToDB = true
                sender.shouldSendToServer = false
                OperationQueue.main.addOperation {
                    sender.send()
                }
            }
        }
    }

    func getThumb(fromAsset asset: AVAsset)->(UIImage?, String?){
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        imageGenerator.appliesPreferredTrackTransform = true
        guard let cgImage = try? imageGenerator.copyCGImage(at: CMTimeMake(0, 1), actualTime: nil)else{return (nil,nil)}
        let image = UIImage(cgImage: cgImage)
        let mediaDirectory = getMediaUrl()!
        let thumbName = "\(NSUUID().uuidString).jpg"
        let thumbFinalURL = mediaDirectory.appendingPathComponent(thumbName)
        guard let thumbData = UIImageJPEGRepresentation(image, 0.5) else {return (nil,nil)}
        if (try? thumbData.write(to: thumbFinalURL)) == nil{
            return (nil,nil)
        }
        return (image,thumbName)
    }

    func processAndSendImage(inAsset asset: PHAsset){
        let manager = PHImageManager()
        let requestOptions = PHImageRequestOptions()
        requestOptions.resizeMode = .exact
        requestOptions.deliveryMode = .highQualityFormat
        requestOptions.isSynchronous = true
        //performing request on background queue as request is synchronous
        DispatchQueue.global(qos: .userInitiated).async {
            manager.requestImage(for: asset, targetSize: imageExportSize , contentMode: .default, options: requestOptions){ image ,info in
                //preparing model object for sending image
                var bondID = ""
                var bondName = ""
                self.fetchedResultsController.managedObjectContext.performAndWait {
                    bondID = self.group.mongoid!
                    bondName = self.group.name!
                }
                //saving image to disk
                guard let image = image else {return}
                guard let imageData = UIImageJPEGRepresentation(image, 0.6) else {return}
                let imageName = NSUUID().uuidString+".jpg"
                guard let imagePath = getMediaUrl()?.appendingPathComponent(imageName) else {return}
                guard ((try? imageData.write(to: imagePath)) != nil) else {return}
                //everything was successful, send to db for now
                let sender = MessageSender(type: "image", text: nil, localMediaURL: imageName, remoteMediaURL: nil, thumbURL: imageName, bondID: bondID, bondName: bondName, thumb: image)
                sender.shouldSendToServer = false
                sender.shouldAddToDB = true
                OperationQueue.main.addOperation {
                    sender.send()
                }
            }
        }
    }
}


//MARK:- CollectionView Delegates
extension ChatlogV2VC: UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let message = (fetchedResultsController.object(at: indexPath) as? Message_CD) else{ return CGSize.zero}
        switch message.type!{
            case "text": //calculate estimated size
                guard let text = message.text else{return CGSize.zero}
                let estimatedFrame = getEstimatedFrame(forText: text)
                return CGSize(width: UIScreen.main.bounds.width, height: estimatedFrame.height + 45)
            case "image", "video": //calculate estimed height
                let imgWidth = fminf(Float(maxBubbleWidth), message.media_width)
                let imgHeight = (CGFloat(imgWidth)/CGFloat(message.media_width)) * CGFloat(message.media_height)
                let allowedHeight = fminf(Float(imgHeight), Float(maxBubbleHeight))
                return CGSize(width: UIScreen.main.bounds.width, height: CGFloat(allowedHeight + 20))
                //maxW/KnownW * KHeihgt
        default: break
        }

        return CGSize.zero
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let message = fetchedResultsController.object(at: indexPath) as! Message_CD
        var cell = UICollectionViewCell()
        switch message.type!{
            case "text": cell = collectionView.dequeueReusableCell(withReuseIdentifier: textMessageIdentifier, for: indexPath)
                        (cell as! TextMessageCell).message = message
                        (cell as! TextMessageCell).parent = self
            case "image": cell = collectionView.dequeueReusableCell(withReuseIdentifier: imageMessageIdentifier, for: indexPath)
                        (cell as! ImageMessageCell).imageView.image = #imageLiteral(resourceName: "image-placeholder") // placeholder image
                        (cell as! ImageMessageCell).message = message
                        (cell as! ImageMessageCell).parent = self
            case "video": cell = collectionView.dequeueReusableCell(withReuseIdentifier: videoMessageIdentifier, for: indexPath)
                        (cell as! VideoMessageCell).imageView.image = #imageLiteral(resourceName: "video-placeholder")
                        (cell as! VideoMessageCell).message = message
                        (cell as! VideoMessageCell).parent = self
            default : break
        }
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let count = fetchedResultsController.sections?[0].numberOfObjects else{return 0}
        return count
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let selectedCell = collectionView.cellForItem(at: indexPath) as? ImageMessageCell{
            let startingFrame = selectedCell.imageView.superview?.convert(selectedCell.imageView.frame, to: nil)
            let zoomingImageView = UIImageView(frame: startingFrame!)
            zoomingImageView.backgroundColor = .black
            zoomingImageView.image = selectedCell.imageView.image
            zoomingImageView.isUserInteractionEnabled = true
            zoomingImageView.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(zoomPanned)))
            zoomingImageView.contentMode = .scaleAspectFit
            if let keyWindow = UIApplication.shared.keyWindow{
                keyWindow.addSubview(zoomingImageView)
                UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: .curveEaseIn, animations: {
                    zoomingImageView.frame = keyWindow.frame
                }, completion: { _ in
                    
                })
            }
        }

        messageTextView.resignFirstResponder()
        bottomConstraint.constant = 0
        UIView.animate(withDuration: 0.3, animations: { 
            self.view.layoutIfNeeded()
        }, completion: nil)
    }

    func zoomPanned(gesture: UIPanGestureRecognizer){
        let imgView = gesture.view
        imgView?.backgroundColor = .clear
        let translation = gesture.translation(in: self.view)
        switch gesture.state{
        case .changed:
            let newPos = CGPoint(x:gesture.view!.center.x + translation.x, y:gesture.view!.center.y + translation.y)
            imgView?.center = newPos
            gesture.setTranslation(CGPoint.zero, in: self.view)
        case .ended: imgView?.removeFromSuperview()
        default: break
        }
    }

}

//MARK: - FRC Delegates
extension ChatlogV2VC: NSFetchedResultsControllerDelegate{

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        if type == .insert{
            scrollFlag = true
            collectionView.insertItems(at: [newIndexPath!])
            let item = fetchedResultsController.object(at: newIndexPath!) as! Message_CD
            if !item.isSender{
                //trigger read notification
                triggerReadNotification(forMessage: item)
            }

        }
        if type == .update{
            scrollFlag = false
            collectionView.reloadItems(at: [newIndexPath!])
        }
    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        let lastItem = self.fetchedResultsController.sections![0].numberOfObjects - 1
        let indexPath = IndexPath(item: lastItem, section: 0)
        if scrollFlag{
            collectionView.scrollToItem(at: indexPath, at: .bottom, animated: true)
        }

        
    }

}

//MARK: - MISC
extension ChatlogV2VC{
    func getEstimatedFrame(forText text: String) -> CGRect{
        let size = CGSize(width: maxBubbleWidth, height: 1000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        let estimatedFrame = NSString(string: text).boundingRect(with: size, options: options, attributes:[NSFontAttributeName: UIFont.systemFont(ofSize: messageTextSize) ], context: nil)
        return estimatedFrame
    }

    func playVideo(forUR url: URL){
        print("playing \(url.absoluteString)")
        let player = AVPlayer(url: url)
        let playerVC = AVPlayerViewController()
        playerVC.player = player
        playerVC.modalPresentationStyle = .overCurrentContext
        DispatchQueue.main.async {
            self.present(playerVC, animated: true) {
                playerVC.player!.play()
            }
        }
 
    }

    func forwardMessage(message: Message_CD){
        let sb = UIStoryboard(name: "Chatlog", bundle: nil)
        let bondChooser = sb.instantiateViewController(withIdentifier: "forwardBondChooser") as! UINavigationController
        (bondChooser.childViewControllers[0] as! ForwardBondChooserVC).msgToForward = message
        bondChooser.modalPresentationStyle = .overCurrentContext
        present(bondChooser, animated: true, completion: nil)
    }

    func triggerReadNotification(forMessage message: Message_CD){
        var messageID: String?
        guard let userInfo = UserDefaults.standard.value(forKey: "userInfo") as? [String:Any] else {return}

        privateContext.performAndWait {
            messageID = message.id
        }

        Alamofire.request(APIEndPoints.confirmRead, method: .post, parameters: ["message_id": messageID!,"id": userInfo["_id"] as! String ], encoding: JSONEncoding.default, headers: nil).responseJSON{response in
            guard let serverResponse = response.value as? [String:Any] else {return}
            print(serverResponse)
        }
    }

}

