//
//  VideoExport.swift
//  FamBond.test
//
//  Created by Jatin Garg on 05/03/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import Foundation
import AVFoundation

class VideoExport: NSObject, ProgressReporting{
    //support implicit progress composition
    var avasset: AVAsset
    var progress: Progress

    var completionHandler: ((_ data: Data?, _ error: NSError?) -> Void)?

    init(avAsset: AVAsset){
        self.avasset = avAsset
        progress = Progress(totalUnitCount: -1)
        progress.kind = .file
        progress.setUserInfoObject(Progress.FileOperationKind.downloading as AnyObject, forKey: ProgressUserInfoKey.fileOperationKindKey)
    }

    func exportVideo(){
        print("exporting video")
        let outputURL = URL(fileURLWithPath: NSTemporaryDirectory() + NSUUID().uuidString + ".mp4")
        let exportSession = AVAssetExportSession(asset: avasset, presetName: AVAssetExportPresetMediumQuality)
        exportSession?.outputURL = outputURL
        exportSession?.outputFileType = AVFileTypeMPEG4
        exportSession?.shouldOptimizeForNetworkUse = true
        exportSession?.exportAsynchronously {
            self.progress.totalUnitCount = 1
            self.progress.completedUnitCount = 1
            guard let session = exportSession else {
                let error = NSError(domain: "Export Session initializing", code: 404, userInfo: nil)
                self.callCompletionHandler(data: nil, error: error)
                return
            }
            switch(session.status){
                case .failed,.unknown,.exporting,.cancelled,.waiting:
                    let error = NSError(domain: "Export session failed exporting", code: 405, userInfo: nil)
                    self.callCompletionHandler(data: nil, error: error)
                case .completed:
                    let data = try? Data(contentsOf: outputURL)
                    //delete file at output path
                    try? FileManager.default.removeItem(at: outputURL)
                    self.callCompletionHandler(data: data, error: nil)
            }

        }
    }

    func callCompletionHandler(data: Data?, error: NSError?){
        completionHandler!(data,error)
        completionHandler = nil
    }
}
