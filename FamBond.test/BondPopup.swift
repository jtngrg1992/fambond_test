//
//  BondPopup.swift
//  FamBond.test
//
//  Created by Jatin Garg on 21/02/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import UIKit
import Alamofire
import CoreData
import BEMCheckBox

class BondPopCell: UITableViewCell{
    var nameLabel: UILabel = {
        let l = UILabel()
        l.textColor = .white
        return l
    }()

    var check: BEMCheckBox = {
        let c = BEMCheckBox()
        c.onFillColor = Globals.instance.sentChatColor
        return c
    }()

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }

    func setup(){
        addSubview(nameLabel)
        addSubview(check)
        backgroundColor = Globals.instance.lightBlue
        nameLabel.anchorCenterYToSuperview()
        check.anchorCenterYToSuperview()
        nameLabel.anchor(nil, left: leftAnchor, bottom: nil, right: check.leftAnchor, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        check.anchor(nil, left: nil, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 10, widthConstant: 30 , heightConstant: 30)
    }
}
class BondPopup: UIView, UITableViewDelegate, UITableViewDataSource{

    var closeBtn: UIButton = {
        let b = UIButton(type: .system)
        b.setTitle("x", for: .normal)
        b.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12)
        b.tintColor = .white
        b.layer.cornerRadius = 15
        b.layer.borderColor = UIColor.white.cgColor
        b.layer.borderWidth = 2
        b.layer.masksToBounds = true
        b.addTarget(self, action: #selector(closeBtnPressed), for: .touchUpInside)
        return b
    }()

    lazy var tableView: UITableView = {
        let t = UITableView()
        t.delegate = self
        t.dataSource = self
        t.backgroundColor = Globals.instance.darkBlue
        t.register(BondPopCell.self, forCellReuseIdentifier: "cell")
        return t
    }()


    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    func closeBtnPressed(){
        self.removeFromSuperview()
    }

    func setup(){
        addSubview(tableView)
        addSubview(closeBtn)
        layer.masksToBounds = true
        layer.cornerRadius = 8

        tableView.anchor(closeBtn.bottomAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        closeBtn.anchorCenterXToSuperview()
        closeBtn.anchor(topAnchor, left: nil, bottom: nil, right: nil, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 10, widthConstant: 30, heightConstant: 30)
        getBonds()

    }

    var bonds: [Group_CD]? = nil{
        didSet{
            tableView.reloadData()
        }
    }
    func getBonds(){
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Group_CD")
        bonds = (try? DBManager.getContext().fetch(fetch)) as? [Group_CD]
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if bonds == nil{
            return 0
        }
        return bonds!.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! BondPopCell
        cell.nameLabel.text = bonds?[indexPath.row].name
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! BondPopCell
        let val = cell.check.on
        cell.check.setOn(!val, animated: true)
    }
}


