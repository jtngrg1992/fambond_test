//
//  VideoCell.swift
//  FamBond.test
//
//  Created by Jatin Garg on 05/03/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

fileprivate var videoMessageKVOContext = 0
class VideoMessageCell: UICollectionViewCell{

    @IBOutlet weak var bubbleView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var playBtn: UIButton!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var deliveryImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!


    @IBOutlet var nameLabelHeightConstraint: NSLayoutConstraint!
    @IBOutlet var bubbleViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet var bubbleViewTrailingConstraint: NSLayoutConstraint!
    @IBOutlet var bubbleViewWidthConstraint: NSLayoutConstraint!

    weak var parent: ChatlogV2VC? = nil
    private var downloadProgressKeyPath = "videoDownload.progress.fractionCompleted"
    private var uploadProgressKeyPath = "videoImport.progress.fractionCompleted"
    private var deliveryKeyPath = "isDelivered"
    private var senderName: String?{
        didSet{
            if let newName = senderName, !message!.isSender{
                nameLabel.text = newName
                nameLabelHeightConstraint.constant = 20
            }else{
                nameLabel.text = ""
                nameLabelHeightConstraint.constant = 0
            }
        }
    }
    private var video: Video?{
        willSet{
            if let formerVideo = video{
                formerVideo.removeObserver(self, forKeyPath: downloadProgressKeyPath)
                formerVideo.removeObserver(self, forKeyPath: uploadProgressKeyPath)
            }
        }
        didSet{
            if let newVideo = video{
                newVideo.addObserver(self, forKeyPath: downloadProgressKeyPath, options: [], context: &videoMessageKVOContext)
                newVideo.addObserver(self, forKeyPath: uploadProgressKeyPath, options: [], context: &videoMessageKVOContext)
                updateUploadProgress()
            }
        }
    }

    var message: Message_CD?{
        willSet{
            //remove observers
            if let formerMessage = message{
                formerMessage.removeObserver(self, forKeyPath: deliveryKeyPath)
            }
        }
        didSet{
            if let newMessage = message{
                newMessage.addObserver(self, forKeyPath: deliveryKeyPath, options: [], context: &videoMessageKVOContext)
                updateBubbleAlignment()
                updateDeliveryStatus()
                manageVideo()
                senderName = getSenderName()
            }
        }
    }


//    override func awakeFromNib() {
//        super.awakeFromNib()
//        let longPresssGesture = UILongPressGestureRecognizer(target: self, action: #selector(longPressed))
//        longPresssGesture.minimumPressDuration = 0.5
//        longPresssGesture.numberOfTapsRequired = 1
//        longPresssGesture.numberOfTouchesRequired = 1
//        imageView.addGestureRecognizer(longPresssGesture)
//    }
//
//    func longPressed(){
//        print("long pressed")
//    }

    @IBAction func playBtnPressed(_ sender: Any) {
        guard let mediaURL = message!.mediaUrl else {return}
            let playBackURL = getMediaUrl()!.appendingPathComponent(mediaURL)
            parent?.playVideo(forUR: playBackURL)
    }

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        guard context == &videoMessageKVOContext
            else{
                super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
                return
        }
        OperationQueue.main.addOperation {
            if keyPath == self.downloadProgressKeyPath{
                self.updateDownloadProgress()
            }
            if keyPath == self.uploadProgressKeyPath{
                self.updateUploadProgress()
            }
            if keyPath == self.deliveryKeyPath{
                self.updateDeliveryStatus()
            }
        }
    }

    func manageVideo(){
        if message!.isSender{
            //needs to upload
            if !message!.isProcessed{
                //message hasn't been already uploaded
                let thumbUrl = getMediaUrl()!.appendingPathComponent(message!.thumbUrl!)
                guard let thumbData = try? Data(contentsOf: thumbUrl) else {return}
                guard let thumbImage = UIImage(data: thumbData) else {return}
                self.imageView.image = thumbImage
                guard let urlString = message?.mediaUrl else {return}
                guard let url = URL(string: urlString) else {return}
                if video != nil{video?.url = url}
                else{video = Video(url: url)}
                if !video!.isUploading{
                    video?.messageID = message!.id!
                    video?.bondID = message!.group!.mongoid!
                    video?.bondName = message!.group!.name!
                    video!.startUpload()
                    video?.isUploading = true
                }
            }else {
                //message is already uploaded, grab from disk and display
                progressView.isHidden = true
                let thumbURL = getMediaUrl()!.appendingPathComponent(message!.thumbUrl!)
                let thumbData = try! Data(contentsOf: thumbURL)
                let thumbImage = UIImage(data: thumbData)!
                imageView.image = thumbImage
            }
        }else{
            //needs to download
            if !message!.isProcessed{
                guard let downloadURLString = message!.text else {return}
                guard let downloadURL = URL(string: downloadURLString) else {return}
                if video != nil {video?.url = downloadURL}
                else {video = Video(url: downloadURL)}
                if !video!.isDownloading{
                    video?.messageID = message!.id!
                    video?.bondID = message!.group!.mongoid
                    video?.bondName = message!.group!.name
                    video?.startDownload()
                    video?.isDownloading = true
                }
            }else{
                //message is already uploaded, grab from disk and display
                progressView.isHidden = true
                let thumbURL = getMediaUrl()!.appendingPathComponent(message!.thumbUrl!)
                let thumbData = try! Data(contentsOf: thumbURL)
                let thumbImage = UIImage(data: thumbData)!
                imageView.image = thumbImage
            }



        }
    }

    private func getSenderName() -> String?{
        guard let senderJSON = message!.sender else {return nil}
        guard let senderData = senderJSON.data(using: .utf8) else {return nil}
        guard let parsedSenderData = (try? JSONSerialization.jsonObject(with: senderData, options: [])) else{return nil}
        guard let senderDictionary = parsedSenderData as? [String:Any] else {return nil}
        guard let senderName = senderDictionary["name"] as? String else {return nil}
        return senderName

    }
    private func updateBubbleAlignment(){
        bubbleViewTrailingConstraint.isActive = message!.isSender
        bubbleViewLeadingConstraint.isActive = !message!.isSender
        bubbleView.backgroundColor = message!.isSender ? Globals.instance.sentChatColor : Globals.instance.receivedChatColor
        //calculating width
        let imgWidth = fminf(Float(maxBubbleWidth), message!.media_width)
        bubbleViewWidthConstraint.constant = CGFloat(imgWidth)
    }

    private func updateDeliveryStatus(){
        if !message!.isSender{
            deliveryImageView.isHidden = true
            return
        }
        deliveryImageView.isHidden = false
        if message!.isDelivered {
            deliveryImageView.image = #imageLiteral(resourceName: "double-tick")
        }else{
            deliveryImageView.image = #imageLiteral(resourceName: "wait")
        }

    }

    private func updateDownloadProgress(){
        if let videoDownload = video?.videoDownload{
            progressView.isHidden = false
            let fraction = videoDownload.progress.fractionCompleted
            progressView.progress = Float(fraction)
        }else{
            progressView.isHidden = true
        }
    }

    private func updateUploadProgress(){
        if let videoImport = video?.videoImport{
            progressView.isHidden = false
            let fraction = videoImport.progress.fractionCompleted
            progressView.progress = Float(fraction)
        }else{
            progressView.isHidden = true
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        bubbleView.layer.shadowColor = UIColor.black.cgColor
        bubbleView.layer.shadowRadius = 1
        bubbleView.layer.shadowOffset = CGSize(width: 0, height: 0)
        bubbleView.layer.shadowOpacity = 0.8
        bubbleView.layer.shouldRasterize = true
        bubbleView.layer.rasterizationScale = UIScreen.main.scale
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(longPressed))
        longPressGesture.minimumPressDuration = 0.4
        bubbleView.addGestureRecognizer(longPressGesture)
    }

    func longPressed(sender: UILongPressGestureRecognizer){
        if sender.state == .began{
            becomeFirstResponder()
            let menuController = UIMenuController.shared
            let forward = UIMenuItem(title: "forward", action: #selector(forwardPressed))
            let copy = UIMenuItem(title: "copy", action: #selector(copyPressed))
            menuController.menuItems = [forward,copy]
            menuController.update()
            menuController.setTargetRect(bubbleView.bounds, in: bubbleView)
            menuController.setMenuVisible(true , animated: true)
        }

    }

    func forwardPressed(){
        parent?.forwardMessage(message: message!)
    }

    func copyPressed(){

    }

    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        super.canPerformAction(action, withSender: sender)
        if action == #selector(copyPressed){
            return true
        }
        if action == #selector(forwardPressed){
            return true
        }
        return false

    }

    override var canBecomeFirstResponder: Bool{
        return true
    }

    
    deinit {
        //remove observers
        message?.removeObserver(self, forKeyPath: deliveryKeyPath)
        video?.removeObserver(self, forKeyPath: downloadProgressKeyPath)
        video?.removeObserver(self, forKeyPath: uploadProgressKeyPath)
    }

}
