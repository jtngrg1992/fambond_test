//
//  PhotoUpload.swift
//  FamBond.test
//
//  Created by Jatin Garg on 02/03/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class MediaUpload: NSObject, ProgressReporting{
    var data: Data
    var progress: Progress
    var completionHandler: ((_ url: String?, _ error: Error?)->Void)?
    let mimeType = "image/jpeg"
    let uploadUrl: URL?

    init(data: Data, uploadURL: URL){
        self.data = data
        self.uploadUrl = uploadURL
        progress = Progress(totalUnitCount: -1)
        progress.kind = ProgressKind.file
        progress.setUserInfoObject(Progress.FileOperationKind.downloading, forKey: .fileOperationKindKey)
    }

    func startUpload(withExtension ext: String){
        Alamofire.upload(multipartFormData: { (mpFD) in
            mpFD.append(self.data, withName: "file", fileName: "yo.\(ext)",mimeType: self.mimeType)
        }, to: self.uploadUrl!) { (result) in
            switch result{
            case .success(let upload,_,_):
                upload.uploadProgress{p in
                    print(p.fractionCompleted)
                    if self.progress.totalUnitCount == -1{
                        self.progress.totalUnitCount = p.totalUnitCount
                    }
                    self.progress.completedUnitCount = p.completedUnitCount
                }
                upload.responseJSON{response in
                    self.progress.completedUnitCount = Int64(self.data.count)
                    switch response.result{
                    case .success(let val):
                        let responseValue = val as? [String:Any]
                        let url = responseValue?["Url"] as? String
                        self.callCompletion(url: url, error: nil)
                    case .failure(let error):
                        self.callCompletion(url: nil, error: error)
                    }
                    }
            case .failure(let error):
                self.callCompletion(url: nil,error : error)
            }

            }

    }



    func callCompletion(url: String? , error: Error?){
        completionHandler?(url,error)
        completionHandler = nil
    }


}
