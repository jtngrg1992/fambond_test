//
//  DataModal.swift
//  FamBond.test
//
//  Created by Jatin Garg on 18/01/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import Foundation
import UIKit


class UserInfo: NSObject{    //to deal with userinfo in local cache
    var _id: String!
    var firstName: String!
    var lastName: String!
    var phone: String!
    var picture: String? = nil
    var dob: String!
    var gender: String!

    init(_id:String,firstName:String,lastName: String,phone:String,picture:String?,dob:String,gender:String){
        super.init()
        self._id = _id
        self.firstName = firstName
        self.lastName = lastName
        self.phone = phone
        self.picture = picture
        self.dob = dob
        self.gender = gender
    }

    func toDict() -> [String : Any]{
        var dict = [String: Any]()
        dict["_id"] = self._id
        dict["firstName"] = self.firstName
        dict["lastName"] = self.lastName
        dict["phone"] = self.phone
        dict["picture"] = self.picture
        dict["dob"] = self.dob
        dict["gender"] = self.gender
        return dict
    }
}
class Message: NSObject{
    var id: String! = nil
    var text: String? = nil
    var isSender: Bool = false
    var receiver: Group_CD? = nil
    var thumbUrl: String? = nil
    var mediaUrl: String? = nil
    var mediaHeight: Float? = nil
    var mediaWidth: Float? = nil
    var type: String! = nil
    var time: Date! = nil
    var delivered: Bool! = false
    var localMediaUrl: String? = nil
    var sender: Int! = nil
    init(dictionary: [String : Any]){
        super.init()
        id = dictionary["id"] as! String
        text = dictionary["text"] as? String
        isSender = dictionary["isSender"] as! Bool
        receiver = dictionary["receiver"] as? Group_CD
        thumbUrl = dictionary["thumbUrl"] as? String
        mediaUrl = dictionary["mediaUrl"] as? String
        mediaWidth = dictionary["mediaWidth"] as? Float
        mediaHeight = dictionary["mediaHeight"] as? Float
        type = dictionary["type"] as! String
        time = dictionary["time"] as! Date
        localMediaUrl = dictionary["mediaUrl"] as? String
        delivered = dictionary["delivered"] as? Bool
        sender = dictionary["sender"] as! Int
    }
    
}

class Group: NSObject{
    var by: Int!
    var id: Int!
    var name: String!
    
    init(dictionary: [String : Any]) {
        super.init()
        by = dictionary["by"] as! Int
        id = dictionary["id"] as! Int
        name = dictionary["name"] as! String
    }
}

class Poll: NSObject{
    var id: String!
    var subject: String!
    var note: String? = nil
    var options = [PollOption]()
    var by: Int!
    var group: Int!
    var time: Date!
    init(dict: [String : Any]){
        super.init()
        subject = dict["subject"] as! String
        note = dict["note"] as? String
        options = dict["options"] as! [PollOption]
        by = dict["by"] as! Int
        group = dict["group"] as! Int
        id = NSUUID().uuidString
        time = dict["time"] as! Date
    }
    
    func toJson() -> [String : Any]{
        var dict = [String : Any] ()
        dict["subject"] = self.subject
        dict["note"] = self.note
        var op = [[String : Any]]()
        for option in self.options{
            op.append(option.toJson())
        }
        dict["options"] = op
        return dict
    }

}

class PollOption: NSObject {
    var text: String!
    var count: Int!
    init(text: String, count: Int){
        super.init()
        self.text = text
        self.count = count
    }
    
    func toJson() -> [String : Any]{
        var dic = [String : Any]()
        dic["text"] = self.text
        dic["count"] = self.count
        return dic
    }
}

class PollInProgress: NSObject {
    var pollID: Int
    var groupID: Int
    var hasResponded: Bool = false

    init(dict: [String : Any]){
        pollID = Int(dict["poollID"] as! String)!
        groupID = Int(dict["groupID"] as! String)!
        hasResponded = dict["hasResponded"] as! Bool
    }


}
