//
//  extensions.swift
//  FamBond.test
//
//  Created by Jatin Garg on 08/01/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import Foundation
import UIKit

extension NSDate{
    class func getDateFromTimeStamp(timeStamp: String)->NSDate?{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let date = formatter.date(from: timeStamp)
        return date as NSDate?
    }
}

extension UIScrollView{
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.subviews.forEach{if let tf = $0 as? UITextField {
            tf.resignFirstResponder()
            }
        }
    }
}
extension String {
    func removingWhitespaces() -> String {
        return components(separatedBy: .whitespaces).joined()
    }
}

extension UIViewController{
    
    //showing activity indeicator view
    func showActivityIndicator() -> OverlayVC{
        let overlay = OverlayVC()
        overlay.animate = true
        overlay.modalPresentationStyle = .overCurrentContext
        present(overlay, animated: false, completion: nil)
        return overlay
    }

    func requestDidFail(withError error: String){

    }

    func requestDidComplete(withResponse: [String:Any]){

    }

    func requestDidComplete(withError: String){
        
    }
}

extension UITextField {
    func setAttributedPlaceholder(string: String){
        let str = NSAttributedString(string: string , attributes: [NSForegroundColorAttributeName : Globals.instance.placeholderColor])
        self.attributedPlaceholder = str
    }

    func shake(){
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.05
        animation.autoreverses = true
        animation.repeatCount = 5
        animation.fromValue = NSValue(cgPoint: CGPoint(x: self.center.x - 4, y: self.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: self.center.x + 4, y: self.center.y))
        self.layer.add(animation, forKey: "position")
    }

    
}
extension UITextView{
    func resolveHashtags(){
        let nsText: NSString = self.text as NSString
        let words = nsText.components(separatedBy: " ")
        let attrs = [
                NSFontAttributeName : UIFont.systemFont(ofSize: standardFontSize)
            ]
        let atrString = NSMutableAttributedString(string: nsText as String, attributes: attrs)
            
            //tag each word if it got hashtag
            for word in words {
                if word.hasPrefix("#"){
                    let matchRange = nsText.range(of: word)
                    //drop the hash tagfrom word
                    let nonHash = String(word.characters.dropFirst())
                    let digits = NSCharacterSet.decimalDigits
                    if let _ = nonHash.rangeOfCharacter(from: digits){
                        //hastag contains a number, disregard
                    }else{
                        atrString.addAttribute(NSLinkAttributeName, value: "hash\(nonHash)", range: matchRange)
                    }
                }
            }
            self.attributedText = atrString
        }
    
    func getRidOfBotIdentifier() -> Bool{
        let words = text.components(separatedBy: " ")
        for word in words{
            if word.hasPrefix("#"){
                    return true
                
            }
        }
        return false
    }
}

extension UIButton{
    
    //nice bounce effect on btn touch
    override open func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        transform = CGAffineTransform.init(scaleX: 1.1, y: 1.1)
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 6, options: .allowUserInteraction, animations: { 
            self.transform = CGAffineTransform.identity
        }, completion: nil)
    }
}
extension UINavigationController{
    
    //ongoing activity indeiator in navigation header
    func showOngoingActivity(){
        let vc = visibleViewController
        let activityHeader = UpdatingNavigationHeader()
        vc?.navigationItem.titleView = activityHeader
        vc?.navigationItem.titleView?.frame.size.height = 40
        vc?.navigationItem.titleView?.frame.size.width = 140
    }
    
    func hideActivityIndicator(with title: String){
        let vc = visibleViewController
        vc?.navigationItem.titleView = nil
        vc?.navigationItem.title = title
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationBar.isTranslucent = false
        self.navigationBar.barTintColor = Globals.instance.navShade
        self.navigationBar.tintColor = .white
        visibleViewController?.navigationItem.titleView?.tintColor = .white
        self.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
    }

}

extension UIView {
    
    //contraints convenience methods
    public func addConstraintsWithFormat(_ format: String, views: UIView...) {
        
        var viewsDictionary = [String: UIView]()
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            viewsDictionary[key] = view
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutFormatOptions(), metrics: nil, views: viewsDictionary))
    }
    
    public func fillSuperview() {
        translatesAutoresizingMaskIntoConstraints = false
        if let superview = superview {
            leftAnchor.constraint(equalTo: superview.leftAnchor).isActive = true
            rightAnchor.constraint(equalTo: superview.rightAnchor).isActive = true
            topAnchor.constraint(equalTo: superview.topAnchor).isActive = true
            bottomAnchor.constraint(equalTo: superview.bottomAnchor).isActive = true
        }
    }
    
    public func anchor(_ top: NSLayoutYAxisAnchor? = nil, left: NSLayoutXAxisAnchor? = nil, bottom: NSLayoutYAxisAnchor? = nil, right: NSLayoutXAxisAnchor? = nil, topConstant: CGFloat = 0, leftConstant: CGFloat = 0, bottomConstant: CGFloat = 0, rightConstant: CGFloat = 0, widthConstant: CGFloat = 0, heightConstant: CGFloat = 0) {
        translatesAutoresizingMaskIntoConstraints = false
        
        _ = anchorWithReturnAnchors(top, left: left, bottom: bottom, right: right, topConstant: topConstant, leftConstant: leftConstant, bottomConstant: bottomConstant, rightConstant: rightConstant, widthConstant: widthConstant, heightConstant: heightConstant)
    }
    
    public func anchorWithReturnAnchors(_ top: NSLayoutYAxisAnchor? = nil, left: NSLayoutXAxisAnchor? = nil, bottom: NSLayoutYAxisAnchor? = nil, right: NSLayoutXAxisAnchor? = nil, topConstant: CGFloat = 0, leftConstant: CGFloat = 0, bottomConstant: CGFloat = 0, rightConstant: CGFloat = 0, widthConstant: CGFloat = 0, heightConstant: CGFloat = 0) -> [NSLayoutConstraint] {
        translatesAutoresizingMaskIntoConstraints = false
        
        var anchors = [NSLayoutConstraint]()
        
        if let top = top {
            anchors.append(topAnchor.constraint(equalTo: top, constant: topConstant))
        }
        
        if let left = left {
            anchors.append(leftAnchor.constraint(equalTo: left, constant: leftConstant))
        }
        
        if let bottom = bottom {
            anchors.append(bottomAnchor.constraint(equalTo: bottom, constant: -bottomConstant))
        }
        
        if let right = right {
            anchors.append(rightAnchor.constraint(equalTo: right, constant: -rightConstant))
        }
        
        if widthConstant > 0 {
            anchors.append(widthAnchor.constraint(equalToConstant: widthConstant))
        }
        
        if heightConstant > 0 {
            anchors.append(heightAnchor.constraint(equalToConstant: heightConstant))
        }
        
        anchors.forEach({$0.isActive = true})
        
        return anchors
    }
    
    public func anchorCenterXToSuperview(constant: CGFloat = 0) {
        translatesAutoresizingMaskIntoConstraints = false
        if let anchor = superview?.centerXAnchor {
            centerXAnchor.constraint(equalTo: anchor, constant: constant).isActive = true
        }
    }
    
    public func anchorCenterYToSuperview(constant: CGFloat = 0) {
        translatesAutoresizingMaskIntoConstraints = false
        if let anchor = superview?.centerYAnchor {
            centerYAnchor.constraint(equalTo: anchor, constant: constant).isActive = true
        }
    }
    
    public func anchorCenterSuperview() {
        anchorCenterXToSuperview()
        anchorCenterYToSuperview()
    }
}



