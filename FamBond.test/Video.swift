//
//  Video.swift
//  FamBond.test
//
//  Created by Jatin Garg on 05/03/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit

class Video: NSObject{
    var data: Data?
    var url: URL?
    var messageID: String!
    var bondID: String!
    var bondName: String!
    var isDownloading = false
    var isUploading = false
    dynamic var videoDownload: MediaDownload?
    dynamic var videoImport: VideoImport?

    init(url: URL) {
        self.url = url
    }

    init(videoData: Data){
        self.data = videoData
    }


    func startDownload() {
        let newDownload = MediaDownload(url: url!)
        newDownload.completionHandler = {(videoData, error) in
            self.isDownloading = false
            self.videoDownload = nil

            //saving video data to local disk
            let videoName = NSUUID().uuidString + ".mp4"
            let videoURL = getMediaUrl()!.appendingPathComponent(videoName)
            try! videoData?.write(to: videoURL) //i know, this will crash it, but i really need it to be there :(

            //extracting thumbnail
            let avAsset = AVAsset(url: videoURL)
            let imageGenerator = AVAssetImageGenerator(asset: avAsset)
            imageGenerator.appliesPreferredTrackTransform = true
            guard let cgImage = try? imageGenerator.copyCGImage(at: CMTimeMake(0, 1), actualTime: nil) else {return}
            let image = UIImage(cgImage: cgImage)
            guard let thumbData = UIImageJPEGRepresentation(image, 0.6) else {return}
            let thumbName = NSUUID().uuidString + ".jpg"
            let thumbURL = getMediaUrl()!.appendingPathComponent(thumbName)
            try! thumbData.write(to: thumbURL)
            //saving to coredata
            DBManager.getContext().perform {
                guard let msg = Message_CD.returnMessage(withId: self.messageID, inContext: DBManager.getContext()) else {return}
                msg.mediaUrl = videoName
                msg.thumbUrl = thumbName
                msg.isProcessed = true
                try? DBManager.getContext().save()
            }

        }
        newDownload.startDownload()
        videoDownload = newDownload
    }

    func startUpload(){
        let avAsset = AVURLAsset(url: self.url!)
        let newVideoImport = VideoImport(asset: avAsset)
        newVideoImport.completionHandler = {localUrl,remoteUrl,error in
            self.isUploading = false
            if !(error != nil){
                //update coredata
                var mediaHeight: Float  = 0
                var mediaWidth: Float = 0
                DBManager.getContext().performAndWait {
                    guard let msg = Message_CD.returnMessage(withId: self.messageID, inContext: DBManager.getContext())
                        else {return}
                    msg.isProcessed = true
                    msg.mediaUrl = localUrl
                    msg.text = remoteUrl
                    mediaWidth = msg.media_width
                    mediaHeight = msg.media_height
                    try? DBManager.getContext().save()
                }

                //dispatch message
                let sender = MessageSender(type: "video", text: nil, localMediaURL: nil, remoteMediaURL: remoteUrl, thumbURL: nil, bondID: self.bondID, bondName: self.bondName, thumb: nil)
                sender.shouldSendToServer = true
                sender.shouldAddToDB = false
                sender.setMediaDimentions(width: mediaWidth, height: mediaHeight)
                sender.setMessageID(id: self.messageID)
                sender.send()
            }else{
                //report
                self.reportError(error: error)
            }
            self.videoImport = nil
        }
        newVideoImport.start()
        videoImport = newVideoImport
    }

    func reportError(error: NSError?){
        print("Error encountered: \(error)")
    }
}
