//
//  ImageCell.swift
//  FamBond.test
//
//  Created by Jatin Garg on 03/03/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import UIKit

private var imageMessageKVOContext = 0
class ImageMessageCell: UICollectionViewCell{

    @IBOutlet weak var deliveryStatusImg: UIImageView!
    @IBOutlet weak var imageView: UIImageView!

    @IBOutlet weak var bubbleView: UIView!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var nameLabelHeightConstraint: NSLayoutConstraint!

    @IBOutlet weak var timeLabel: UILabel!

    var parent: ChatlogV2VC? = nil
    private var imageKeyPath = "image"
    private var downloadCompletedKeyPath = "photoDownload.progress.fractionCompleted"
    private var uploadCompletedKeyPath = "photoUpload.progress.fractionCompleted"
    private var isDeliveredKeyPath = "isDelivered"
    private var senderName: String?{
        didSet{
            if let newName = senderName, !message!.isSender{
                nameLabel.text = newName
                nameLabelHeightConstraint.constant = 20
            }else{
                nameLabel.text = ""
                nameLabelHeightConstraint.constant = 0
            }
        }
    }
    
    var photo: Photo?{
        willSet{
            if let formerPhoto = photo{
                formerPhoto.removeObserver(self, forKeyPath: imageKeyPath)
                formerPhoto.removeObserver(self, forKeyPath: downloadCompletedKeyPath)
                formerPhoto.removeObserver(self, forKeyPath: uploadCompletedKeyPath)

            }
        }didSet{
            if let newPhoto = photo{
                newPhoto.addObserver(self, forKeyPath: imageKeyPath, options: [], context: &imageMessageKVOContext)
                newPhoto.addObserver(self, forKeyPath: downloadCompletedKeyPath, options: [], context: &imageMessageKVOContext)
                newPhoto.addObserver(self, forKeyPath: uploadCompletedKeyPath, options: [], context: &imageMessageKVOContext)

            }
            updateImageView()
            updateDownloadProgress()
            updateUploadProgress()
        }
    }

    @IBOutlet var bubbleWidthConstraint: NSLayoutConstraint!
    @IBOutlet var bubbleTrailingConstraint: NSLayoutConstraint!
    @IBOutlet var bubbleLeadingConstraint: NSLayoutConstraint!

    var message: Message_CD?{
        willSet{
            if let formerMesage = message{
                //removeObservers
                formerMesage.removeObserver(self, forKeyPath: isDeliveredKeyPath)
            }
        }
        didSet{
            if let newMessage = message{
                //add obbsrvers
                newMessage.addObserver(self, forKeyPath: isDeliveredKeyPath, options: [], context: &imageMessageKVOContext)
                senderName = getSenderName()
                updateDelivery()
                updateBubbleAlignment()
                managePhoto()
                updateTime()
            }

        }
    }


    private func getSenderName() -> String?{
        guard let senderJSON = message!.sender else {return nil}
        guard let senderData = senderJSON.data(using: .utf8) else {return nil}
        guard let parsedSenderData = (try? JSONSerialization.jsonObject(with: senderData, options: [])) else{return nil}
        guard let senderDictionary = parsedSenderData as? [String:Any] else {return nil}
        guard let senderName = senderDictionary["name"] as? String else {return nil}
        return senderName

    }

    func managePhoto(){
        if message!.isSender{
            //upload
            guard let imageURL = getMediaUrl()?.appendingPathComponent(message!.mediaUrl!) else {return}
            //obtaining image file
            guard let imageData = try? Data(contentsOf: imageURL) else {return}
            //there is a better solution for this but..
            guard let image = UIImage(data: imageData) else{return}

            if !message!.isProcessed{
                //need to upload now
                if photo == nil {photo = Photo(image: image)}
                else{photo?.image = image}
                photo?.messageID = message!.id! //for updating isProcessing status
                photo?.bondId = message!.group!.mongoid!
                photo?.bondName = message!.group!.name!
                if !photo!.isUploading{photo?.startUpload();print("started uploading")}
            }else{
                //photo is already uploaded
                progressView.isHidden = true
                imageView.image = image
            }
        }else{
            //download
            //checks if the photo is already downloaded
            if !message!.isProcessed{
                guard let downloadUrl = URL(string: message!.text!) else{return}
                if photo == nil{photo = Photo(url: downloadUrl)}
                else{photo?.url = downloadUrl}
                photo?.messageID = message!.id!
                if !photo!.isDownloading{_=photo?.startDownload();print("starting download")}
            }else{
                //photo is already downloaded
                progressView.isHidden = true
                DBManager.getContext().perform {
                    guard let imageURL = getMediaUrl()?.appendingPathComponent(self.message!.mediaUrl!) else{return}
                    guard let imageData = try? Data(contentsOf: imageURL) else{return}
                    guard let image = UIImage(data: imageData) else{return}
                    OperationQueue.main.addOperation {
                        self.imageView.image = image
                    }
                }
            }
        }
    }

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        guard context == &imageMessageKVOContext else{
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
            return
        }
        OperationQueue.main.addOperation {
            if keyPath == self.imageKeyPath{
                self.updateImageView()
            }
            if keyPath == self.downloadCompletedKeyPath{
                self.updateDownloadProgress()
            }
            if keyPath == self.uploadCompletedKeyPath{
                self.updateUploadProgress()
            }
            if keyPath == self.isDeliveredKeyPath{
                self.updateDelivery()
            }
        }
    }

    private func updateDelivery(){
        if !message!.isSender{
            deliveryStatusImg.isHidden = true
            return
        }
        deliveryStatusImg.isHidden = false
        if message!.isDelivered {
            deliveryStatusImg.image = #imageLiteral(resourceName: "double-tick")
        }else{
            deliveryStatusImg.image = #imageLiteral(resourceName: "wait")
        }
    }

    private func updateTime(){
        guard let time = message!.time else {return}
        let formattedTme = formatMesageTime(date: time as Date)
        timeLabel.text = formattedTme
    }

    func updateUploadProgress(){
        if let photoUpload = photo?.photoUpload{
            progressView.isHidden = false
            let fraction = photoUpload.progress.fractionCompleted
            progressView.progress = Float(fraction)
        }else{
            progressView.isHidden = true
        }
    }

    func updateDownloadProgress(){
        if let photoDownload = photo?.photoDownload{
            progressView.isHidden = false
            let fraction = photoDownload.progress.fractionCompleted
            progressView.progress = Float(fraction)
        }else{
            progressView.isHidden = true
        }

    }

    func updateImageView(){
        if let image = photo?.image{
            imageView.image = image
        }else{
            imageView.image = #imageLiteral(resourceName: "image-placeholder")
        }
    }

    func updateBubbleAlignment(){
        bubbleTrailingConstraint.isActive = message!.isSender
        bubbleLeadingConstraint.isActive = !message!.isSender
        bubbleView.backgroundColor = message!.isSender ? Globals.instance.sentChatColor : Globals.instance.receivedChatColor
        //calculating width
        let imgWidth = fminf(Float(maxBubbleWidth), message!.media_width)
        self.bubbleWidthConstraint.constant = CGFloat(imgWidth)
     }

    override func awakeFromNib() {
        super.awakeFromNib()
        bubbleView.layer.shadowColor = UIColor.black.cgColor
        bubbleView.layer.shadowRadius = 1
        bubbleView.layer.shadowOffset = CGSize(width: 0, height: 0)
        bubbleView.layer.shadowOpacity = 0.8
        bubbleView.layer.shouldRasterize = true
        bubbleView.layer.rasterizationScale = UIScreen.main.scale
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(longPressed))
        longPressGesture.minimumPressDuration = 0.4
        bubbleView.addGestureRecognizer(longPressGesture)
    }

    func longPressed(sender: UILongPressGestureRecognizer){
        if sender.state == .began{
            becomeFirstResponder()
            let menuController = UIMenuController.shared
            let forward = UIMenuItem(title: "forward", action: #selector(forwardPressed))
            let copy = UIMenuItem(title: "copy", action: #selector(copyPressed))
            menuController.menuItems = [forward,copy]
            menuController.update()
            menuController.setTargetRect(bubbleView.bounds, in: bubbleView)
            menuController.setMenuVisible(true , animated: true)
        }

    }

    func forwardPressed(){
        parent?.forwardMessage(message: message!)
    }

    func copyPressed(){

    }

    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        super.canPerformAction(action, withSender: sender)
        if action == #selector(copyPressed){
            return true
        }
        if action == #selector(forwardPressed){
            return true
        }
        return false

    }

    override var canBecomeFirstResponder: Bool{
        return true
    }


    deinit{
        photo?.removeObserver(self, forKeyPath: imageKeyPath)
        photo?.removeObserver(self, forKeyPath: downloadCompletedKeyPath)
        photo?.removeObserver(self, forKeyPath: uploadCompletedKeyPath)
        message?.removeObserver(self, forKeyPath: isDeliveredKeyPath)
    }
}
