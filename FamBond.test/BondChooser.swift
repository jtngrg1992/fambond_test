//
//  BondChooser.swift
//  FamBond.test
//
//  Created by Jatin Garg on 06/03/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class ForwardBondChooserCell: UITableViewCell{

    @IBOutlet weak var bondImage: UIImageView!

    @IBOutlet weak var bondName: PriorityLabel!

    override func awakeFromNib() {
        bondName.isSelected = false
        bondName.selectIndicator.layer.cornerRadius = 20
        bondName.font = UIFont.systemFont(ofSize: 15)
    }
}


class ForwardBondChooserVC: UITableViewController{

    var bonds: [Group_CD]?{
        didSet{
            tableView.reloadData()
        }
    }
    
    var msgToForward: Message_CD!

    var selectedBonds = [Group_CD]()

    override func viewDidLoad() {
        super.viewDidLoad()
        getBonds()

    }

    func getBonds(){
        bonds = Group_CD.returnAllGroups(context: DBManager.getContext())
    }


    @IBAction func donePressed(_ sender: Any) {
        if selectedBonds.count == 0{
            present(Globals.instance.showMessage(title: "Attention", message: "Please select at least one bond to forward the message to", actionHandler: nil)
                , animated: true, completion: nil)
            return
        }

        //constructing thumbImage
        var image: UIImage?
        guard let type = msgToForward.type else {return}
        switch type{
            case "image","video":
                guard let thumbImage = getThumbImage(fromUrl: msgToForward.thumbUrl)
                    else {
                        //there is no way this message can be forwarded
                        present(Globals.instance.showMessage(title: "Error", message: "There was an error in reading this message", actionHandler: nil), animated: true, completion: nil)
                        dismiss(animated: true, completion: nil)
                        return
                    }
                image = thumbImage
            default : break
        }

        selectedBonds.forEach{bond in
            let sender = MessageSender(type: msgToForward.type, text: msgToForward.text, localMediaURL: msgToForward.mediaUrl, remoteMediaURL: msgToForward.text, thumbURL: msgToForward.thumbUrl, bondID: bond.mongoid, bondName: bond.name, thumb: image)
            sender.shouldAddToDB = true
            sender.shouldSendToServer = true
            sender.send()

        }
        navigationController?.dismiss(animated: true, completion: nil)
    }

    func getThumbImage(fromUrl url: String?) -> UIImage?{
        guard let url = url else {return nil}
        guard let fullThumbURL = getMediaUrl()?.appendingPathComponent(url) else {return nil}

        guard let thumbData = (try? Data(contentsOf: fullThumbURL)) else {return nil}
        guard let thumbImage = UIImage(data: thumbData) else {return nil}
        return thumbImage
    }

    @IBAction func cancelPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if bonds == nil{
            return 0
        }
        return bonds!.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ForwardBondChooserCell
        if let image = bonds![indexPath.row].picture{
            if let url = URL(string: image){
                cell.bondImage.sd_setImage(with: url)
            }
        }
        if let bondName = bonds![indexPath.row].name{
            cell.bondName.text = bondName
        }
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! ForwardBondChooserCell
        cell.bondName.isSelected = !cell.bondName.isSelected
        if cell.bondName.isSelected{
            //need to insert
            selectedBonds.append(bonds![indexPath.row])
        }else{
            //need to remove from selections
            selectedBonds.remove(at: selectedBonds.index(of: bonds![indexPath.row])!)
        }
    }
}
