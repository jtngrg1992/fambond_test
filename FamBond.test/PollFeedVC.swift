//
//  PollFeedVC.swift
//  FamBond.test
//
//  Created by Jatin Garg on 21/02/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import UIKit
import Charts
import Alamofire

class ChartFormatter: NSObject, IAxisValueFormatter{
    var xLabels: [String]!
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        if (Int(value)<xLabels.count){
            return xLabels[Int(value)]
        }else{
            return "blah"
        }

    }
}
class PollFeedCell: UITableViewCell, ChartViewDelegate{

    @IBOutlet weak var pollBtn: UIButton!
    @IBOutlet weak var pollLabel: UILabel!
    @IBOutlet weak var barChartView: BarChartView!

    @IBAction func pollBtnPressed(_ sender: Any) {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let optionChooser = sb.instantiateViewController(withIdentifier: "pollOptionChooser") as! PollOptionChooserVC
        optionChooser.poll = self.poll
        optionChooser.selfID = delegate?.selfID
        delegate?.navigationController?.pushViewController(optionChooser, animated: true)

    }
    var poll: [String:Any]? = nil
    weak var delegate: PollFeedVC? = nil

    func setChart(dataPoints: [String],values: [Double]){
        var dataEntries: [BarChartDataEntry] = []
        barChartView.noDataText = "Enter Data"
        let formatter = ChartFormatter()
        formatter.xLabels = dataPoints
        for i in 0..<dataPoints.count {
            formatter.stringForValue(Double(i), axis: barChartView.xAxis)
            let dataEntry = BarChartDataEntry(x: Double(i), y: values[i])
            dataEntries.append(dataEntry)
        }
        let chartDataSet = BarChartDataSet(values: dataEntries, label: "")
        chartDataSet.colors = [Globals.instance.darkBlue]
        let chartData = BarChartData(dataSets: [chartDataSet])
        barChartView.data = chartData
        barChartView.xAxis.valueFormatter = formatter
//        barChartView.xAxis.labelCount = Int((barChartView.data?.yMax)! > Double(dataPoints.count) ? Double(dataPoints.count) : barChartView.data!.yMax);
        barChartView.xAxis.labelCount = dataPoints.count
    }


    override func awakeFromNib() {
        super.awakeFromNib()
        barChartView.delegate = self
        barChartView.xAxis.labelPosition = .top
        barChartView.chartDescription?.text = ""
        barChartView.animate(yAxisDuration: 1.0)
        barChartView.legend.enabled = false
        barChartView.drawBordersEnabled = false
        barChartView.dragEnabled = false
        barChartView.pinchZoomEnabled = false
        barChartView.leftAxis.drawGridLinesEnabled = false
        barChartView.rightAxis.drawGridLinesEnabled = false
        barChartView.xAxis.drawGridLinesEnabled = false
        barChartView.drawGridBackgroundEnabled = false
        barChartView.xAxis.wordWrapEnabled = true
        barChartView.doubleTapToZoomEnabled = false
        let yaxis = barChartView.getAxis(YAxis.AxisDependency.left)
        yaxis.drawLabelsEnabled = false
        yaxis.enabled = false

        let xaxis = barChartView.getAxis(YAxis.AxisDependency.right)
        xaxis.drawLabelsEnabled = false
        xaxis.enabled = false
    }

    override func prepareForReuse() {
        super.prepareForReuse()
    }
}

class PollFeedVC: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let userInfo = UserDefaults.standard.value(forKey: "userInfo") as! [String:Any]
        selfID = userInfo["_id"] as! String
    }

    var polls: [[String : Any]]?{
        didSet{
            print(polls?.count)
            tableView.reloadData()
        }
    }

    var selfID: String!

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //getting poll data
        let userInfo = UserDefaults.standard.value(forKey: "userInfo") as! [String:Any]
        let _id = userInfo["_id"] as! String
        Alamofire.request(APIEndPoints.getPolls, method: .post, parameters: ["_id":_id], encoding: JSONEncoding.default, headers: nil).responseJSON{
            response in
            let serverResponse = response.value as? [String : Any]
            guard let success = serverResponse?["success"] as? Bool else{
                DispatchQueue.main.async {
                    self.present( Globals.instance.showMessage(title: "Attention", message: "An error was encountered, please try again later", actionHandler: nil), animated: true, completion: nil)
                }
                return
            }
            if success{
                let data = serverResponse?["data"] as! [[String : Any]]
                self.polls = data
            }else{
                DispatchQueue.main.async {
                    self.present( Globals.instance.showMessage(title: "Attention", message: "An error was encountered, please try again later", actionHandler: nil), animated: true, completion: nil)
                }
            }
        }

    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if polls == nil{
            return 0
        }
        return polls!.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! PollFeedCell
        var dataPoints = [String]()
        var values = [Double]()
        (polls![indexPath.row]["options"] as! [[String:Any]]).forEach { (option) in
            dataPoints.append(option["name"] as! String)
            values.append(option["count"] as! Double)
        }
        cell.setChart(dataPoints: dataPoints, values: values)
        cell.pollLabel.text = polls![indexPath.item]["title"] as? String
        cell.poll = polls![indexPath.row]
        cell.delegate = self
        cell.pollBtn.isEnabled = true
        cell.pollBtn.alpha = 1

        //deciding whether user has already votes
        guard let votes = (polls![indexPath.row]["votes"]) as? [[String:Any]]else{return cell}
        var hasVoted = false
        for vote in votes{
            guard let by = vote["by"] as? String else{break}
            if selfID.trimmingCharacters(in: .whitespacesAndNewlines) == by.trimmingCharacters(in: .whitespacesAndNewlines){
                hasVoted = true
            }

        }
        if hasVoted{
            cell.pollBtn.isEnabled = false
            cell.pollBtn.alpha = 0.4
        }
        return cell
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let optionCount = (polls![indexPath.row]["options"] as! [[String:Any]]).count
        let size = (optionCount * 30) + 50
        return CGFloat(size)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? PollOptionChooserVC{
            destination
        }
    }




}


