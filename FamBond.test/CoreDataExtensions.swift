//
//  CoreDataExtensions.swift
//  FamBond.test
//
//  Created by Jatin Garg on 02/02/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import Foundation
import CoreData

extension Contact{
    class func insertContact(withInfo info: [String:Any],context: NSManagedObjectContext){
        let phone = info["phone"] as! String
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Contact")
        fetch.predicate = NSPredicate(format: "phone=%@", phone)
        let contact = (try? context.fetch(fetch)) as? [Contact]
        if let contacts = contact, contacts.count == 0{
            //need to insert
            print("inserting \(info["name"] as! String)")
            let contact = NSEntityDescription.insertNewObject(forEntityName: "Contact", into: context) as! Contact
            contact.id = info["_id"] as? String
            contact.phone = info["phone"] as? String
            contact.name = info["name"] as? String
            contact.picture = info["picture"] as? String
        }
    }

    class func getContacts(context: NSManagedObjectContext) -> [Contact]?{
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Contact")
        fetch.predicate = NSPredicate(format: "id!=nil")
        let contacts = (try? context.fetch(fetch)) as? [Contact]
        return contacts
    }
}

extension Message_CD{

    class func returnMessage(withId id: String, inContext context: NSManagedObjectContext) -> Message_CD?{
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Message_CD")
        fetch.predicate = NSPredicate(format: "id = %@", id)
        let results = (try? context.fetch(fetch)) as? [Message_CD]
        return results?.first
    }

    //releant
    class func insertMessage(withInfo info: [String:Any], context: NSManagedObjectContext){
        let msg = NSEntityDescription.insertNewObject(forEntityName: "Message_CD", into: context) as! Message_CD
        msg.id = info["id"] as? String
        if let group = Group_CD.returnGroupFromGroupId(id: info["bondid"] as! String, withContext: context){
            group.addToMessages(msg)
        }
        msg.isDelivered = info["isDelivered"] as! Bool
        //serializing sender data
        if let senderdata = try? JSONSerialization.data(withJSONObject: info["sender"] as! [String:Any], options: .prettyPrinted){
            if let senderString = NSString(data: senderdata, encoding: String.Encoding.utf8.rawValue){msg.sender = senderString as String}

        }

        msg.text = info["text"] as? String
        let data = try! JSONSerialization.data(withJSONObject: info["sender"] as! [String:Any], options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        msg.sender = jsonString as? String
        if let senderStatus = info["isSender"] as? Bool{
            msg.isSender = senderStatus
        }
        msg.time = info["time"] as? NSDate
        msg.mediaUrl = info["mediaUrl"] as? String
        msg.thumbUrl = info["thumbUrl"] as? String
        msg.type = info["type"] as? String
        if let mediaHeight = info["mediaHeight"] as? Float, let mediaWidth = info["mediaWidth"] as? Float{
            msg.media_height = mediaHeight
            msg.media_width = mediaWidth
        }
        
        }

    class func insertOfflineMessage(withInfo info: [String:Any],context: NSManagedObjectContext){
        let msg = NSEntityDescription.insertNewObject(forEntityName: "Message_CD", into: context) as! Message_CD
        msg.id = info["id"] as? String
        if let group = Group_CD.returnGroupFromGroupId(id: info["bondid"] as! String, withContext: context){
            group.unreadCount += 1
            group.addToMessages(msg)
        }
        msg.isDelivered = true
        msg.text = info["text"] as? String
        let data = try! JSONSerialization.data(withJSONObject: info["sender"] as! [String:Any], options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        msg.sender = jsonString as? String
        if let senderStatus = info["isSender"] as? Bool{
            msg.isSender = senderStatus
        }
        let time = Double(info["timestamp"] as! String)
        let date = Date(timeIntervalSince1970: time!/1000)
        msg.time = date as NSDate?
        msg.mediaUrl = info["mediaUrl"] as? String
        msg.thumbUrl = info["thumbUrl"] as? String
        msg.type = info["type"] as? String
        if let mediaHeight = info["mediaHeight"] as? Float, let mediaWidth = info["mediaWidth"] as? Float{
            msg.media_height = mediaHeight
            msg.media_width = mediaWidth
        }

    }

    }

extension Group_CD{
    class func returnGroupFromGroupId(id: String, withContext context: NSManagedObjectContext) -> Group_CD?{
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Group_CD")
        fetch.predicate = NSPredicate(format: "mongoid=%@", id)
        let results = ((try? context.fetch(fetch)) as? [Group_CD])
        return results?.first
    }
    

    class func createBondUsingData(data:[String:Any],context: NSManagedObjectContext){
        let _id = data["_id"] as! String
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Group_CD")
        fetch.predicate = NSPredicate(format: "mongoid=%@", _id)
        let results = (try? context.fetch(fetch)) as? [Group_CD]
        if results?.first == nil{
            let bond = NSEntityDescription.insertNewObject(forEntityName: "Group_CD", into: context) as? Group_CD
            bond?.mongoid = _id
            bond?.name = data["name"] as? String
            bond?.created_by = data["created_by"] as? String
            let date = NSDate.getDateFromTimeStamp(timeStamp:data["timestamp"] as! String)
            bond?.timestamp = date
            bond?.picture = data["picture"] as? String
            bond?.unreadCount = 0
        }
    }

    class func incrementUnreadCounter(id: String,context: NSManagedObjectContext){
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName:"Group_CD")
        fetch.predicate = NSPredicate(format: "mongoid=%@", id)
        let group = (try? context.fetch(fetch)) as? [Group_CD]
        group?.first?.unreadCount+=1
    }

    

    class func returnAllGroups(context: NSManagedObjectContext) -> [Group_CD]?{
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Group_CD")
        let groups = (try? context.fetch(fetch)) as? [Group_CD]
        return groups
    }
}

extension SOSContact{
    class func returnSOSContacts(withContext context: NSManagedObjectContext) -> [SOSContact]?{
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "SOSContact")
        let contacts = (try? context.fetch(fetch)) as? [SOSContact]
        return contacts
    }

    class func insertSOSCOntact(withContext context: NSManagedObjectContext, contact: Contact){
        let sos = NSEntityDescription.insertNewObject(forEntityName: "SOSContact", into: context) as! SOSContact
        sos.id = contact.id
        sos.name = contact.name
        sos.phone = contact.phone
        sos.picture = contact.picture
    }
}
