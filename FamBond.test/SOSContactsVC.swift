//
//  SOSContactsVC.swift
//  FamBond.test
//
//  Created by Jatin Garg on 26/02/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import UIKit
import CoreData

class SOSContactsCell: UITableViewCell{
    @IBOutlet weak var nameLabel: PriorityLabel!
    @IBOutlet weak var phoneLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        nameLabel.isSelected = false
    }
}

class SOSContactsVC: UITableViewController {
    var contacts: [Contact]? = nil{
        didSet{
            if contacts != nil{
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }

            }

        }
    }
    var selectedContacts = [Contact]()
    override func viewDidLoad() {
        navigationItem.title = "Choose SOS Contacts"
        super.viewDidLoad()
        let rightButton = UIBarButtonItem(title: "Save", style: .done, target: self, action: #selector(savePressed))
        navigationItem.rightBarButtonItem = rightButton
        loadData()
     }

    func savePressed(){
        if selectedContacts.count == 0{
            present(Globals.instance.showMessage(title: "Error", message: "You need to select at least one contact to add to SOS list", actionHandler: nil), animated: true, completion: nil)
            return
        }
        selectedContacts.forEach {
            SOSContact.insertSOSCOntact(withContext: DBManager.getContext(), contact: $0)
            do{
                try DBManager.getContext().save()
            }catch let error{
                present(Globals.instance.showMessage(title: "Attention", message: "An error was encountered", actionHandler: nil), animated: true, completion: nil)
                print(error)
            }
        }
        _=navigationController?.popViewController(animated: true)
    }

    func loadData(){
        self.contacts = Contact.getContacts(context: DBManager.getContext())
    }

}

extension SOSContactsVC{
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if contacts == nil{
            return 0
        }
        return contacts!.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! SOSContactsCell
        var name = ""
        var phone = ""
        name = self.contacts![indexPath.row].name!
        phone = self.contacts![indexPath.row].phone!
        cell.nameLabel.text = name
        cell.phoneLabel.text = phone
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! SOSContactsCell
        let value = !cell.nameLabel.isSelected
        if value{
            selectedContacts.append(contacts![indexPath.row])
        }else{
            let index = selectedContacts.index(of: contacts![indexPath.row])
            selectedContacts.remove(at: index!)
        }
        cell.nameLabel.isSelected = value
        print(selectedContacts.count)
    }
}
