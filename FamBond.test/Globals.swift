//
//  Globals.swift
//  FamBond.test
//
//  Created by Jatin Garg on 05/01/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

let serverBase = "http://52.221.229.53:8000"
struct UserDefaultKeys{
    static let userInfo = "userInfo"
    static let chatWallpaper = "chatWallPaper"
}

struct APIEndPoints{
    static let register = "\(serverBase)/register"
    static let checkRegisteration = "\(serverBase)/isRegistered"
    static let getBonds = "\(serverBase)/getBonds"
    static let getContacts = "\(serverBase)/getcontacts"
    static let getPolls = "\(serverBase)/getPolls"
    static let sendPollResponse = "\(serverBase)/sendPollResponse"
    static let createPoll = "\(serverBase)/createPoll"
    static let createStickie = "\(serverBase)/createStickie"
    static let getStickies = "\(serverBase)/getStickies"
    static let createBond = "\(serverBase)/createGroup"
    static let bondInfo = "\(serverBase)/getGroupInfo"
    static let addBondMember = "\(serverBase)/addMember"
    static let sendDeviceToken = "\(serverBase)/setDeviceToken"
    static let getOfflineMessages = "\(serverBase)/getMessages"
    static let logout = "\(serverBase)/logout"
    static let confirmRead = "\(serverBase)/confirmRead"
    static let getReaders = "\(serverBase)/getReaders"
}

func getMediaUrl() -> URL?{
    let fileManager = FileManager.default
    do{
        let docUrl = try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        let mediaUrl = docUrl.appendingPathComponent("media")
        return mediaUrl
    }catch let error {
        print("Couldn't get media url with error: \(error)")
    }
    return nil
}

func getOfflineMessages(){
    if let userInfo = UserDefaults.standard.value(forKey: "userInfo") as? [String:Any]{
        let _id = userInfo["_id"] as! String
        Alamofire.request(APIEndPoints.getOfflineMessages, method: .post, parameters: ["_id": _id], encoding: JSONEncoding.default, headers: nil).responseJSON{response in
            let serverResponse = response.value as? [String:Any]
            guard let success = serverResponse?["success"] as? Bool else{return}
            if success{
                let messages = serverResponse?["data"] as! [[String:Any]]
                for message in messages{
                    DBManager.getContext().perform {
                        _=Message_CD.insertOfflineMessage(withInfo: message, context: DBManager.getContext())
                        try? DBManager.getContext().save()
                    }

                }
            }
        }
    }

}

func logout(completionHandler: ((Bool)->Void)?){
    var completionHandler = completionHandler
    guard let userInfo = UserDefaults.standard.value(forKey: "userInfo") as? [String:Any] else{completionHandler!(false); completionHandler = nil; return }
    guard let _id = userInfo["_id"] as? String else {completionHandler!(false); completionHandler = nil; return}
    Alamofire.request(APIEndPoints.logout, method: .post, parameters: ["id":_id], encoding: JSONEncoding.default, headers: nil).responseJSON{response in
        guard let serverResponse = response.value as? [String:Any] else {completionHandler!(false); completionHandler = nil; return}
        guard let success = serverResponse["success"] as? Bool else {completionHandler!(false); completionHandler = nil; return }
        completionHandler!(success)
    }

}

func checkForEmptyText(inTextFields textFields: [FTextField]) -> FTextField?{
    for tf in textFields{
        tf.strokeColor = .white
        if (tf.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)!{
            return tf
        }
    }
    return nil
}


func makePostRequest(url: String, parameters: [String:Any],handler: @escaping (Bool,Any)->Void){
    Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON{response in
        guard let serverResponse = response.value as? [String:Any] else{
            DispatchQueue.main.async {
                handler(false,[:])
            }
            return
        }
        guard let success = serverResponse["success"] as? Bool else{
            DispatchQueue.main.async {
                handler(false,[:])
            }
            return
        }
        if success{
            guard let data = serverResponse["data"] else{
                DispatchQueue.main.async {
                    handler(false,[:])
                }
                return
            }
            DispatchQueue.main.async {
                handler(true,data)
            }

        }
    }
}

class Globals: NSObject{
    static let instance = Globals()
    
    //Colors
    let darkBlue: UIColor = UIColor(colorLiteralRed: 23/255, green: 94/255, blue: 123/255, alpha: 1)
    let lightBlue: UIColor = UIColor(colorLiteralRed: 33/255, green: 100/255, blue: 128/255, alpha: 1)
    let navShade: UIColor = UIColor(colorLiteralRed: 20/255, green: 81/255, blue: 106/255, alpha: 1)
//    let pBlueShade: UIColor = .white
    //let sBlueShade: UIColor = .white
    let secondColor: UIColor = UIColor(colorLiteralRed: 131/255, green: 195/255, blue: 65/255, alpha: 1)
    let grayShade: UIColor = UIColor(colorLiteralRed: 130/255, green: 150/255, blue: 159/255, alpha: 1)
    let placeholderColor: UIColor = UIColor(colorLiteralRed: 152/255, green: 162/255, blue: 167/255, alpha: 1)
    let sentChatColor: UIColor = UIColor(colorLiteralRed: 241/255, green: 241/255, blue: 241/255, alpha: 1)
    let receivedChatColor: UIColor = UIColor(colorLiteralRed: 129/255, green: 198/255, blue: 58/255, alpha: 1)
    
    //fonts
    let systemFont: UIFont = UIFont(name: "Avenir Next", size: 15)!
    
    let activeGroupID: String? = nil
    //convenience methods
    
    func showMessage(title: String, message: String, actionHandler: UIAlertAction?) -> UIAlertController{
        let alert = UIAlertController(title: title , message: message, preferredStyle: .alert)
        if let action = actionHandler{
            alert.addAction(action)
        }else{
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        }
        
        return alert
    }
    
    
    func getJsonString(dictionary: [String : Any]) -> String {
        let json = try? JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted)
        let jsonString = NSString(data: json!, encoding: String.Encoding.utf8.rawValue) as! String
        return jsonString
    }


    func getGraident() -> UIImage {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        let color2 = UIColor.black.withAlphaComponent(0.2)
        gradientLayer.colors = [Globals.instance.darkBlue, color2].map{$0.cgColor}
        gradientLayer.startPoint = CGPoint(x: 0.2, y: 0.2)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.2)
        UIGraphicsBeginImageContext(gradientLayer.bounds.size)
        gradientLayer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    func createSubDirInDocs(subDir: String){
        let fileManager = FileManager.default
        let path = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).strings(byAppendingPaths: [subDir])[0]
        if !fileManager.fileExists(atPath: path){
            do{
                try fileManager.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
                print("Created path: \(path)")
            }
            catch let error{
                print("Error : \(error)")
            }
            
        }else{
            print("Path already exists")
        }
    }
    func setupNavHeader(navigationController: UINavigationController?, navigationItem: UINavigationItem!){
        
        navigationItem.titleView?.frame = CGRect(x: 0, y: 0, width: (navigationController?.navigationBar.frame.width)!, height: (navigationController?.navigationBar.frame.height)!)
        let activityHeader = UpdatingNavigationHeader()
        navigationItem.titleView = activityHeader
        navigationItem.titleView?.frame.size.height = 40
        navigationItem.titleView?.frame.size.width = 140
    }
}

class FTextField: UITextField{
    var strokeColor: UIColor = UIColor.white{
        didSet{
            setNeedsDisplay()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    var index: Int? = nil
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        backgroundColor = UIColor.clear
        borderStyle = .none
        font = Globals.instance.systemFont
        textColor = UIColor.white
        translatesAutoresizingMaskIntoConstraints = false
    }
    
    override func draw(_ rect: CGRect) {
        let path = UIBezierPath()
        let startingPoint = CGPoint(x: bounds.origin.x, y: bounds.origin.y + bounds.height)
        path.move(to: startingPoint)
        strokeColor.setStroke()
        path.lineWidth = 2
        path.addLine(to: CGPoint(x: startingPoint.x + bounds.width, y: startingPoint.y))
        path.stroke()
    }

}

class Fbutton: UIButton{
    var cornerRadius: CGFloat = 0{
        didSet{
            setNeedsDisplay()
        }
    }
    override func draw(_ rect: CGRect) {
        let path = UIBezierPath(roundedRect: rect, cornerRadius: cornerRadius)
        UIColor.white.setStroke()
        path.lineWidth = 2
        UIColor.clear.setFill()
        path.fill()
        path.stroke()
    }
    
    override init(frame: CGRect){
        super.init(frame: frame)
        setup()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    func setup(){
        titleEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10)
        setTitleColor(UIColor.white, for: .normal)
        titleLabel?.font = UIFont(name: "Avenir Next", size: 15)
    }

    
}
