//
//  Album.swift
//  FamBond.test
//
//  Created by Jatin Garg on 02/03/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import Foundation
import UIKit

class Album: NSObject{
    var photoUrls: [URL]?{
        didSet{
            //initialize using latest url
            if let urls = photoUrls{
                let index = urls.count - 1
                self.photos.append(Photo(url: urls[index]))
            }
        }
    }
    var photos: [Photo]

    override init() {
        photoUrls = [URL]()
        photos = [Photo]()
    }


    func isUrlInAlbum(url: URL)->Bool{
        guard let urls = photoUrls else{return false}
        for photoUrl in urls{
            if photoUrl.absoluteString == url.absoluteString{
                return true
            }
        }
        return false
    }

    func getPhotoInstance(withUrl url: URL) -> Photo?{
        for photo in photos{
            if photo.url == url{
                return photo
            }
        }
        return nil
    }

    func setMessageId(forURL url: URL, messageID: String){
        for photo in self.photos{
            if photo.url == url{
                photo.messageID = messageID
                return
            }
        }
    }
}
