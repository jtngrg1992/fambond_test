//
//  PhotoImport.swift
//  FamBond.test
//
//  Created by Jatin Garg on 02/03/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import Foundation
import UIKit

class MediaDownload: NSObject, ProgressReporting, URLSessionDelegate, URLSessionDownloadDelegate{
    var downloadUrl: URL
    var completionHandler: ((_ data: Data?, _ error: Error?)->Void)?
    var progress: Progress

    init(url: URL){
        self.downloadUrl = url
        progress = Progress(totalUnitCount: -1)
        progress.kind = ProgressKind.file
        progress.setUserInfoObject(Progress.FileOperationKind.downloading, forKey: .fileOperationKindKey)
    }

    func startDownload(){
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config, delegate: self, delegateQueue: nil)
        let task = session.downloadTask(with: self.downloadUrl)
        task.resume()

    }

    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        print("Downloaded \(totalBytesWritten) / \(totalBytesExpectedToWrite) bytes ")
        if !(progress.totalUnitCount > 0){
            progress.totalUnitCount = totalBytesExpectedToWrite
        }
        progress.completedUnitCount = totalBytesWritten
    }

    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        let data = try? Data(contentsOf: location)
//        let image = UIImage(data: data!)
        progress.completedUnitCount = Int64(data!.count)
        callCompletionHandler(data: data, error: nil)
        session.invalidateAndCancel()
    }

    func callCompletionHandler(data: Data?,error: Error?){
        completionHandler?(data,error)
        completionHandler = nil
    }
}
