//
//  CalendarVC.swift
//  FamBond.test
//
//  Created by Jatin Garg on 13/02/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import UIKit
import JTAppleCalendar
import Alamofire

class UpcomingEventsCell: UITableViewCell{
    @IBOutlet weak var subjectLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
}

class CalendarVC: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var calendarView: JTAppleCalendarView!
    var formatter = DateFormatter()
    var scheduleDates = [String]()
    var stickies = [[String:Any]](){
        didSet{
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.markCallendar()
                self.calendarView.reloadData()
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
        calendarView.dataSource = self
        calendarView.delegate = self
        calendarView.registerCellViewXib(file: "CalenderCell")
        setHeaderView()
        askForStickies()

    }

    func markCallendar(){
    self.stickies.forEach{
        let d = $0["scheduled_date"] as! String
        self.scheduleDates.append(d)
        }
    }

    func askForStickies(){
        let userInfo = UserDefaults.standard.value(forKey: "userInfo") as! [String:Any]
        let _id = userInfo["_id"] as! String
        Alamofire.request(APIEndPoints.getStickies, method: .post, parameters: ["_id":_id], encoding: JSONEncoding.default, headers: nil).responseJSON{response in
            guard let serverResponse = response.value as? [String:Any] else{
                DispatchQueue.main.async {
                    self.present(Globals.instance.showMessage(title: "Attention",message: "Some error was encountered, try again later", actionHandler: nil), animated: true, completion: nil)
                }
                return
            }
            guard let success = serverResponse["success"] as? Bool else{
                DispatchQueue.main.async {
                    self.present(Globals.instance.showMessage(title: "Attention", message: "Some error was encountered, try again later", actionHandler: nil), animated: true, completion: nil)
                }
                return
            }
            if success{
                self.stickies = serverResponse["data"] as! [[String:Any]]
            }else{
                let error = serverResponse["errors"] as! String
                DispatchQueue.main.async {
                    self.present(Globals.instance.showMessage(title: "Attention",message: "\(error)", actionHandler: nil), animated: true, completion: nil)
                }
            }
        }
    }

    func setHeaderView(){
        formatter.dateFormat = "MMMM"
        let currentMonth = formatter.string(from: Date())
        monthLabel.text = currentMonth
        formatter.dateFormat = "yyyy"
        yearLabel.text = formatter.string(from: Date())
    }

    @IBAction func jumpNextMonth(_ sender: Any) {
        calendarView.scrollToSegment(.next)
    }

    @IBAction func jumpPreviousMonth(_ sender: Any) {
        calendarView.scrollToSegment(.previous)
    }
}

extension CalendarVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stickies.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let stickie = stickies[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! UpcomingEventsCell
        cell.subjectLabel.text = stickie["subject"] as? String
        //formatting timestamp
        let timeString = stickie["scheduled_time"] as! String
        var dateString = stickie["scheduled_date"] as! String
        formatter.dateFormat = "MM-dd-yyyy"
        let date = formatter.date(from: dateString)
        formatter.dateFormat = "MMMM dd"
        dateString = formatter.string(from: date!)
        cell.dateLabel.text = "\(dateString) \(timeString)"
        return cell
    }
    
}
//MARK: - Calendar Delegates

extension CalendarVC: JTAppleCalendarViewDelegate,JTAppleCalendarViewDataSource{

    public func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        formatter.dateFormat = "yyyy MM dd"
        let startDate = Date()
        let c = NSCalendar(calendarIdentifier: .gregorian)
        var components = DateComponents()
        components.day = 60
        let endDate = c?.date(byAdding: components, to: startDate)
        let parameters = ConfigurationParameters(startDate: startDate, endDate: endDate!, numberOfRows: 4, calendar: Calendar.current, generateInDates: .forAllMonths, generateOutDates: .tillEndOfGrid, firstDayOfWeek: .sunday, hasStrictBoundaries: true)
        return parameters
    }

    func calendar(_ calendar: JTAppleCalendarView, willDisplayCell cell: JTAppleDayCellView, date: Date, cellState: CellState) {
        let myCustomCell = cell as! CalendarCell

        //setup cell text
        myCustomCell.dayLabel.text = cellState.text
        //setup color
        if cellState.dateBelongsTo == .thisMonth{
            myCustomCell.dayLabel.textColor = .black
        }
        else{
            myCustomCell.dayLabel.textColor = .gray
        }
        //converting date to string for comparison
        formatter.dateFormat = "MM-dd-yyyy"
        let strDate = formatter.string(from: date)
        if scheduleDates.index(of: strDate) != nil{
            myCustomCell.marker.isHidden = false
            print("found")
        }else{
            myCustomCell.marker.isHidden = true
        }
    }

    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        let firstDate = visibleDates.monthDates[0]
        formatter.dateFormat = "MMMM"
        let monthString = formatter.string(from: firstDate)
        let year =  NSCalendar.current.component(.year, from: firstDate)
        monthLabel.text = monthString
        yearLabel.text = "\(year)"

    }
}
