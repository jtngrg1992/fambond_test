//
//  ActivityOverlay.swift
//  FamBond.test
//
//  Created by Jatin Garg on 08/01/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView

class OverlayVC: UIViewController {
    
    var activityIndicator: NVActivityIndicatorView = {
        let n = NVActivityIndicatorView(frame: CGRect.zero, type: .ballClipRotate, color: .white, padding: 30)
        return n
    }()
    
    var animate: Bool = false{
        didSet{
            if animate{
                activityIndicator.startAnimating()
            }else{
                activityIndicator.stopAnimating()
                self.dismiss(animated: false, completion: nil)
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        view.addSubview(activityIndicator)
        activityIndicator.anchorCenterYToSuperview()
        activityIndicator.anchorCenterXToSuperview()
    }
}
