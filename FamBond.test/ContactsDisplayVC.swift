//
//  ContactsDisplayVC.swift
//  FamBond.test
//
//  Created by Jatin Garg on 21/02/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import UIKit
import Contacts
import Alamofire
import SDWebImage
import CoreData
import MessageUI
class MyCell: UITableViewCell{
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var inviteBtn: UIButton!

    var contact: [String:Any]? = nil
    weak var delegate: ContactsDisplayVC? = nil
    @IBAction func invitePressed(_ sender: Any) {
        delegate?.inviteBtnPressed(forNumber: contact?["phone"] as! String)
    }
}
class ContactsDisplayVC: UITableViewController,InviteModalViewDelegate,MFMessageComposeViewControllerDelegate {

    var contactsStore = CNContactStore()
    let keysToFetch = [
        CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
        CNContactPhoneNumbersKey] as [Any]
    var displayContacts = [[String: Any]]()

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return displayContacts.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! MyCell
        cell.delegate = self
        cell.contact = displayContacts[indexPath.row] 
        if let picture = displayContacts[indexPath.row]["picture"] as? String {
            if let pictureUrl = URL(string: picture){
                cell.profileImage.sd_setImage(with: pictureUrl)
            }
        }else{
            cell.profileImage.image = #imageLiteral(resourceName: "placeholder")
        }
        if let _ = displayContacts[indexPath.row]["_id"] as? String{
            cell.inviteBtn.isHidden = true
        }else{
            cell.inviteBtn.isHidden = false
        }

        cell.nameLabel.text = displayContacts[indexPath.row]["name"] as? String
        return cell
    }

    var tintView: UIView!

    func inviteBtnPressed(forNumber number: String){
        if let keyWindow = UIApplication.shared.keyWindow{
            tintView = UIView(frame: .zero)
            tintView.frame = keyWindow.bounds
            tintView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
            keyWindow.addSubview(tintView)
            let inviteModal = InviteModalView(frame: CGRect.zero)
            inviteModal.recipients = [number]
            inviteModal.delegate = self
            tintView.addSubview(inviteModal)
            inviteModal.anchorCenterXToSuperview()
            inviteModal.anchorCenterYToSuperview()
            inviteModal.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.8).isActive = true
            inviteModal.heightAnchor.constraint(equalToConstant: 200).isActive = true
            inviteModal.transform = CGAffineTransform(scaleX: 0.2, y: 0.9)
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                inviteModal.transform = CGAffineTransform.identity
            }, completion: nil)
        }
    }

    func modalClosed() {
        tintView.removeFromSuperview()
    }

    func whatsAppPressed(){
        tintView.removeFromSuperview()
        let whatsAppURL = NSURL.init(string: "whatsapp://send?text=Install%20FamBond%20now!")
        if UIApplication.shared.canOpenURL(whatsAppURL as! URL){
            UIApplication.shared.openURL(whatsAppURL as! URL)
        }
        else{
            present(Globals.instance.showMessage(title: "Error", message: "Whatsapp is not installed on this device", actionHandler: nil), animated: true, completion: nil)
        }
    }

    func messagePressed(numbers: [String]){
        tintView.removeFromSuperview()
        if MFMessageComposeViewController.canSendText(){
            let controller = MFMessageComposeViewController()
            controller.body = "install Fambond now!"
            controller.messageComposeDelegate = self
            controller.recipients = numbers
            present(controller, animated: true, completion: nil)
        }
        else{
            present(Globals.instance.showMessage(title: "Error", message: "Unable to launch messaging", actionHandler: nil), animated: true, completion: nil)
        }
     }

    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true, completion: nil)
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupAddressBook()
    }


    func verifyAgainstServer(){
        var phones = [String]()
        for contact in displayContacts{
            phones.append(contact["phone"] as! String)
        }
        Alamofire.request(APIEndPoints.getContacts, method: .post, parameters: ["phones":phones], encoding: JSONEncoding.default, headers: nil).responseJSON{response in
            let serverResponse = response.value as? [String : Any]
            guard let success = serverResponse?["success"] as? Bool else{
                DispatchQueue.main.async {
                    self.present( Globals.instance.showMessage(title: "Attention", message: "An error was encountered, please try again later", actionHandler: nil), animated: true, completion: nil)
                }
                return
            }
            if success{
                let data = serverResponse?["data"] as! [[String:Any]]
                print(data)
                //setting flags
                for contact in data{
                    let phoneToCompare = contact["phone"] as! String
                    var index = 0
                    for displayContact in self.displayContacts{
                        let phoneToCompareAgainst = displayContact["phone"] as! String
                        if phoneToCompare == phoneToCompareAgainst{
                            self.displayContacts[index]["_id"] = contact["_id"]
                            self.displayContacts[index]["picture"] = contact["picture"] as? String
                            break
                        }else{
                            index += 1
                        }
                    }
                }
                self.tableView.reloadData()
                //making coredata entry
                let pvtContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
                pvtContext.parent = DBManager.getContext()
                 pvtContext.perform {
                    for displayContact in self.displayContacts{
                        Contact.insertContact(withInfo: displayContact, context: pvtContext)
                    }
                    try? pvtContext.save()
                    DBManager.getContext().performAndWait {
                        try? DBManager.getContext().save()
                    }
                }

            }else{
                DispatchQueue.main.async {
                    self.present( Globals.instance.showMessage(title: "Attention", message: "An error was encountered, please try again later", actionHandler: nil), animated: true, completion: nil)
                }
            }
        }
    }
    func setupAddressBook(){
        contactsStore.requestAccess(for: .contacts, completionHandler: {(success,error) -> Void in
            if success{
                let predicate = CNContact.predicateForContactsInContainer(withIdentifier: self.contactsStore.defaultContainerIdentifier())
                var contacts: [CNContact]! = []
                do {
                    contacts = try self.contactsStore.unifiedContacts(matching: predicate, keysToFetch: self.keysToFetch as! [CNKeyDescriptor])// [CNContact]
                }catch {
                    let alert = Globals.instance.showMessage(title: "Error", message: "Error Fetching your contacts, plesse try again later", actionHandler: nil)
                    self.present(alert, animated: true, completion: nil)
                }
                self.displayContacts = [[String:Any]]()
                for contact in contacts{
                    var phoneStr = ""
                    var nameStr = ""
                    var number: CNPhoneNumber!
                    if contact.phoneNumbers.count > 0{
                        number = contact.phoneNumbers[0].value
                        phoneStr = number.stringValue.replacingOccurrences(of: "-", with: "")
                        phoneStr = phoneStr.replacingOccurrences(of: "(", with: "")
                        phoneStr = phoneStr.replacingOccurrences(of: ")", with: "")
                        phoneStr = phoneStr.trimmingCharacters(in: .whitespacesAndNewlines)
                    }
                    nameStr = contact.familyName + contact.givenName
                    if !(nameStr.isEmpty) && !(phoneStr.isEmpty){
                        self.displayContacts.append(["phone": phoneStr.removingWhitespaces(), "name": nameStr])
                    }
                }
                self.verifyAgainstServer()

            }else{
                self.present(Globals.instance.showMessage(title: "Alert!", message: "You have prevented fambond from accessing your contacts, you might have to add friends manually", actionHandler: nil), animated: true, completion: nil)
            }
        })
        }

}
